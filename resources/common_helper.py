#from .authenticate import Auth
from uuid import getnode
from getmac import get_mac_address
from cryptography.fernet import Fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
import os
import datetime
from dateutil import relativedelta
import base64
import socket
import requests
import json
import shutil
import psutil
import platform
import netifaces
import subprocess
import time
import certifi
import subprocess
import urllib3
import traceback
import random
from PIL import Image
from io import BytesIO
from glob import glob
from .camera_controller import *
import cv2
from uuid import uuid4
from models.model_query import AVCCModel
import configparser
class Helper:


    @classmethod
    def get_varient(cls) :
        return 'INT-AVCC'
		
    @classmethod
    def get_version(cls) :
        return '1.1'
	
    @classmethod
    def get_product_unique_id(cls) :
        return 'INT21-AVCCNPR1'
	
    @classmethod
    def get_ip(cls) :	
        iface = netifaces.gateways()['default'][netifaces.AF_INET][1]
        ip_addr = netifaces.ifaddresses(iface)[netifaces.AF_INET][0]['addr']
        #data = {'ip':ip_addr}
        
        return ip_addr
    
    @classmethod
    def get_wifi_eth_ip(cls) :	
        try:
            wifi = ""
            eth =""
            eth = subprocess.check_output("ifconfig eth0 | grep inet | awk '{}'".format("{ print $2 }"), shell=True).decode("utf-8").split("\n")[0]
            wifi =  subprocess.check_output("ifconfig wlan0 | grep inet | awk '{}'".format("{ print $2 }"), shell=True).decode("utf-8").split("\n")[0]
            #print(eth , wifi)
            return wifi , eth
        except Exception as e:
            #Log.resources_log.error('Wifi eth error : '+str(e))
            return wifi , eth	

    @classmethod
    def get_socket_ip_port(cls)	:
        try:
            eth = ""
            port = ""
            eth = subprocess.check_output("ifconfig eth0 | grep inet | awk '{}'".format("{ print $2 }"), shell=True).decode("utf-8").split("\n")[0]
            port = AVCCModel.get_config_data()[0]['socket_port']
            return eth ,port
        except Exception as e:
            print(e)
            return eth  , port

    @classmethod
    def get_ssid(cls) :	
        connected_wifi = "NONE"
        try:
            status = subprocess.check_output('nmcli radio wifi', shell=True)
            status = status.decode('utf-8')
            status = status.split("\n")[0]
            if status == "disabled":
                #os.system('nmcli r wifi on')
                return {'status': 300, 'message': 'Wifi Disabled',}

            connected_wifi = subprocess.check_output('iwgetid -r', shell=True)
            connected_wifi = connected_wifi.decode('utf-8')
            connected_wifi = connected_wifi.split("\n")[0]
        except Exception as ex:
            #Log.resources_log.error('SSID error : '+str(ex))
            pass

        #data = {'connected_ssid':connected_wifi}
        return connected_wifi
	
    @classmethod
    def get_mac(cls) :
        #mac_hex = hex(getnode())
        #mac = mac_hex.split('x')[1]
        eth_mac = str(get_mac_address(interface="eth0"))
        mac = eth_mac.replace(":", "")
        return mac
	

    @classmethod
    def crypt_key(cls): 
        salt = b'!nT*8Awu#4wqs6%@sxe' 
        kdf = PBKDF2HMAC(
                algorithm=hashes.SHA256(),
                length=32,
                salt=salt,
                iterations=100000,
                backend=default_backend()
                )
        password_provided = "!nt0zi@pi$face#app" # This is input in the form of a string
        password = password_provided.encode()
        key = base64.urlsafe_b64encode(kdf.derive(password))
        return key
        
    @classmethod
    def get_temp_with_memory(cls):
        try:
            temp = subprocess.check_output("cat /sys/devices/virtual/thermal/thermal_zone*/temp",shell=True)
            temp = int(int(temp.decode().split('\n')[0])/1000)

            hdd_space_perct = int(psutil.disk_usage('/').percent)
            #print(hdd_space_perct , temp)
            return hdd_space_perct , temp
        except Exception as ex:
            #Log.resources_log.error('Temp Mem error : ',str(e))
            pass

    
            
    @classmethod
    def get_device_unique_key(cls) :
        """
        stream = os.popen('cat /proc/cpuinfo | grep ^Serial | cut -d":" -f2')
        serial = stream.read()
        serial = serial.split(" ")[1]
        return "INTZ"+str(serial.strip())
        """
        cpu_id = subprocess.check_output('cat /proc/device-tree/serial-number',shell=True)
        cpu_id = cpu_id.decode().rstrip('\x00')
        return "INTZ"+cpu_id
        #return "INTZ060401"

    @classmethod    
    def validate_camera(cls , data_payload):
        try:
            camera_data = AVCCModel.validate_camera(data_payload['camera_url'] , data_payload['camera_type'])
            
            if camera_data[0]['count']==0:
                
                
            
                if str(data_payload['camera_url']).isnumeric():
                    capture_obj = cv2.VideoCapture(int(data_payload['camera_url']))
                else:
                    capture_obj = cv2.VideoCapture(data_payload['camera_url'])
                if capture_obj.isOpened():
                    capture_obj.release()
                    return True ,"None"
                else:
                    return False , "Camera is not Available"
            else:
                return False , "Camera already exists"
                
        except Exception as cam_error:
            print(cam_error)
            return False ,cam_error


    @classmethod
    def list_usb_cameras(cls):
        devs = os.listdir('/dev')
        capture_resources = [dev for dev in devs if dev.startswith('video')]
        video_devices = sorted(capture_resources)
        return video_devices
    
    @classmethod
    def update_version(cls):
        try:
            if cls.is_connected():
                product_unique_id=cls.get_product_unique_id()
                api_endpoint = "http://api.intozi.io/v1/api/get_intozi_edge_device_version/"
                api_key = "144278c3bcb2127eccca0ac1aaf799a84a6aa048"
                data = {'product_unique_id':product_unique_id}
                #print(data)
                api_res = requests.post(api_endpoint, headers = {'key':api_key}, data=data)
                response_dump = json.loads(api_res.text)
                
                if response_dump['status'] == 201 :
                    #print("updated version")
                    version=response_dump['version']
                    data = AVCCModel.update_version(version)
            else:
                print('No internet connection')
        except Exception as ex:
            #Log.cloud_log.error("Updating version"+str(ex))
            print(ex)
    
    @classmethod
    def is_connected(cls):
        try:
            socket.create_connection(("www.google.com", 80))
            return True

        except OSError as e:
            #logging.error("Connecting to internet error")
            #logging.exception(e)
            pass
        return False

    @classmethod
    def manage_profile(cls):
        config = configparser.ConfigParser()
        if os.path.exists('profile.ini'):
            config.read('profile.ini')
            if not os.path.exists(config['DEFAULT']['BUS_MASK_PATH']):
                bus_mask = cv2.imread(os.path.join(os.getcwd() , 'files' , 'bus.jpg'))
                bus_mask_name = "{}.jpg".format(str(uuid4()) )
                bus_mask_path = os.path.join(os.environ['BASE_IMAGE_PATH'] ,bus_mask_name)
                cv2.imwrite(bus_mask_path , bus_mask)
                config['DEFAULT']['BUS_MASK_PATH'] = "/images/"+bus_mask_name
            print(config['DEFAULT']['BUS_MASK_PATH'])

            if not os.path.exists(config['DEFAULT']['TRUCK_MASK_PATH']):
                truck_mask = cv2.imread(os.path.join(os.getcwd() , 'files' , 'truck.jpg'))
                truck_mask_name = "{}.jpg".format(str(uuid4()) )
                truck_mask_path = os.path.join(os.environ['BASE_IMAGE_PATH'] ,truck_mask_name)
                cv2.imwrite(truck_mask_path , truck_mask)
                config['DEFAULT']['TRUCK_MASK_PATH'] ="/images/"+ truck_mask_name
            print(config['DEFAULT']['TRUCK_MASK_PATH'])

            if not os.path.exists(config['DEFAULT']['TRIPLE_TRUCK_MASK_PATH']):
                triple_truck_mask = cv2.imread(os.path.join(os.getcwd() , 'files' , 'truck2.jpg'))
                triple_truck_mask_name = "{}.jpg".format(str(uuid4()) )
                triple_truck_mask_path = os.path.join(os.environ['BASE_IMAGE_PATH'] , triple_truck_mask_name)
                cv2.imwrite(triple_truck_mask_path , triple_truck_mask)
                config['DEFAULT']['TRIPLE_TRUCK_MASK_PATH'] = "/images/"+triple_truck_mask_name
            print(config['DEFAULT']['TRIPLE_TRUCK_MASK_PATH'])
        else:
            config['DEFAULT'] = {'BUS_MASK_PATH': 'None','TRUCK_MASK_PATH': 'None','TRIPLE_TRUCK_MASK_PATH': 'None'}

            bus_mask = cv2.imread(os.path.join(os.getcwd() , 'files' , 'bus.jpg'))
            bus_mask_name = "{}.jpg".format(str(uuid4()) )
            bus_mask_path = os.path.join(os.environ['BASE_IMAGE_PATH'] ,bus_mask_name)
            cv2.imwrite(bus_mask_path , bus_mask)
            config['DEFAULT']['BUS_MASK_PATH'] = "/images/"+bus_mask_name
            print(config['DEFAULT']['BUS_MASK_PATH'])

            truck_mask = cv2.imread(os.path.join(os.getcwd() , 'files' , 'truck.jpg'))
            truck_mask_name = "{}.jpg".format(str(uuid4()) )
            truck_mask_path = os.path.join(os.environ['BASE_IMAGE_PATH'] ,truck_mask_name)
            cv2.imwrite(truck_mask_path , truck_mask)
            config['DEFAULT']['TRUCK_MASK_PATH'] ="/images/"+ truck_mask_name
            print(config['DEFAULT']['TRUCK_MASK_PATH'])
            
            triple_truck_mask = cv2.imread(os.path.join(os.getcwd() , 'files' , 'truck2.jpg'))
            triple_truck_mask_name = "{}.jpg".format(str(uuid4()) )
            triple_truck_mask_path = os.path.join(os.environ['BASE_IMAGE_PATH'] , triple_truck_mask_name)
            cv2.imwrite(triple_truck_mask_path , triple_truck_mask)
            config['DEFAULT']['TRIPLE_TRUCK_MASK_PATH'] = "/images/"+triple_truck_mask_name
            print(config['DEFAULT']['TRIPLE_TRUCK_MASK_PATH'])


            with open('profile.ini', 'w') as configfile:
                config.write(configfile)
        """
        if not os.path.exists(config['BUS_MASK_PATH']):
            bus_mask = cv2.imread(os.path.join(os.getcwd() , 'files' , 'bus.jpg'))
            bus_mask_path = os.path.join(config['BASE_IMAGE_PATH'] ,)
            cv2.imwrite(bus_mask_path , bus_mask)
            config['BUS_MASK_PATH'] = bus_mask_path
            print(config['BUS_MASK_PATH'])

        if not os.path.exists(config['TRUCK_MASK_PATH']):
            truck_mask = cv2.imread(os.path.join(os.getcwd() , 'files' , 'truck.jpg'))
            truck_mask_path = os.path.join(config['BASE_IMAGE_PATH'] ,)
            cv2.imwrite(truck_mask_path , truck_mask)
            config['TRUCK_MASK_PATH'] = bus_mask_path
            print(config['TRUCK_MASK_PATH'])

        if not os.path.exists(config['TRIPLE_TRUCK_MASK_PATH']):
            triple_truck_mask = cv2.imread(os.path.join(os.getcwd() , 'files' , 'truck2.jpg'))
            triple_truck_mask_path = os.path.join(config['BASE_IMAGE_PATH'] ,)
            cv2.imwrite(triple_truck_mask_path , triple_truck_mask)
            config['TRIPLE_TRUCK_MASK_PATH'] = triple_truck_mask_path
            print(config['TRIPLE_TRUCK_MASK_PATH'])
        """

    @classmethod
    def memory_management(cls):
        try:
            hdd = psutil.disk_usage('/')

            total = int(hdd.total / (2**30))
            used = int(hdd.used / (2**30))
            percent = int(100*used/total)

            
            print("-----------------"  , percent)
            if percent > 90:
                deletable_dir = AVCCModel.get_deletable_dir()
                print(deletable_dir)
                for dir in deletable_dir:
                    images_path =os.path.join(os.environ['BASE_IMAGE_PATH'] , dir)
                    video_path =os.path.join(os.environ['BASE_IMAGE_PATH'] , dir+"_video")
                    print("Removing :: ",images_path , video_path)
                    if os.path.exists(images_path):
                        shutil.rmtree(images_path)
                        print("removing path " , images_path)  
                    
                    if os.path.exists(video_path):    
                        shutil.rmtree(video_path)  
                    AVCCModel.delete_data_datewise(date=dir)
                    os.system("sudo chmod 777 "+os.environ['BASE_IMAGE_PATH'])
                    os.system("sudo chmod 777 "+os.environ['BASE_IMAGE_PATH']+"/*")
                    break

        except Exception as e:
            print(e)

    @classmethod
    def profile_generator(cls , vehicle_class, axle_count  ):
        base_url = "/var/www/html"
        profile_image_path = ""

        """
        if class_id == 3:
            profile_image_dir = "/images/CJV"
            image_name = random.choice(os.listdir(base_url+profile_image_dir))
            profile_image_path = profile_image_dir+"/"+image_name

        elif class_id==4:
            profile_image_dir = "/images/LCV"
            image_name = random.choice(os.listdir(base_url + profile_image_dir))
            profile_image_path = profile_image_dir+"/"+image_name
        elif class_id == 5:
            profile_image_dir = "/images/BUS"
            image_name = random.choice(os.listdir(base_url + profile_image_dir))
            profile_image_path = profile_image_dir+"/"+image_name
        elif class_id == 6:
            profile_image_dir = "/images/2_AXLE_TRUCK"
            image_name = random.choice(os.listdir(base_url + profile_image_dir))
            profile_image_path = profile_image_dir+"/"+image_name
        elif class_id == 7:
            profile_image_dir = "/images/3_AXLE_TRUCK"
            image_name = random.choice(os.listdir(base_url + profile_image_dir))
            profile_image_path = profile_image_dir+"/"+image_name
        
        elif class_id == 8:
            if axle_count==4:
                profile_image_dir = "/images/4_AXLE_TRUCK"
                image_name = random.choice(os.listdir(base_url + profile_image_dir))
                profile_image_path = profile_image_dir+"/"+image_name
            elif axle_count==5:
                profile_image_dir = "/images/5_AXLE_TRUCK"
                image_name = random.choice(os.listdir(base_url + profile_image_dir))
                profile_image_path = profile_image_dir+"/"+image_name
            else :
                profile_image_dir = "/images/2_AXLE_TRUCK"
                image_name = random.choice(os.listdir(base_url + profile_image_dir))
                profile_image_path = profile_image_dir+"/"+image_name
        """
        
        if vehicle_class == 'Hatchback':
            profile_image_dir = "/images/HATCHBACK"
            image_name = random.choice(os.listdir(base_url + profile_image_dir))
            profile_image_path = profile_image_dir+"/"+image_name
        elif vehicle_class == 'Jeep':
            profile_image_dir = "/images/JEEP"
            image_name = random.choice(os.listdir(base_url + profile_image_dir))
            profile_image_path = profile_image_dir+"/"+image_name
        elif vehicle_class == 'LCV':
            profile_image_dir = "/images/LCV"
            image_name = random.choice(os.listdir(base_url + profile_image_dir))
            profile_image_path = profile_image_dir+"/"+image_name

        elif vehicle_class == 'MAV':
            if axle_count==4:
                profile_image_dir = "/images/4_AXLE_TRUCK"
                image_name = random.choice(os.listdir(base_url + profile_image_dir))
                profile_image_path = profile_image_dir+"/"+image_name
            elif axle_count==5:
                profile_image_dir = "/images/5_AXLE_TRUCK"
                image_name = random.choice(os.listdir(base_url + profile_image_dir))
                profile_image_path = profile_image_dir+"/"+image_name
            else:
                profile_image_dir = "/images/6_AXLE_TRUCK"
                image_name = random.choice(os.listdir(base_url + profile_image_dir))
                profile_image_path = profile_image_dir+"/"+image_name
           

        elif vehicle_class == 'Mini Bus':
            profile_image_dir = "/images/MINI_BUS"
            image_name = random.choice(os.listdir(base_url + profile_image_dir))
            profile_image_path = profile_image_dir+"/"+image_name
        elif vehicle_class == 'Mini Truck':
            profile_image_dir = "/images/MINI_TRUCK"
            image_name = random.choice(os.listdir(base_url + profile_image_dir))
            profile_image_path = profile_image_dir+"/"+image_name
        elif vehicle_class == 'Pickup Truck':
            profile_image_dir = "/images/PICKUP_TRUCK"
            image_name = random.choice(os.listdir(base_url + profile_image_dir))
            profile_image_path = profile_image_dir+"/"+image_name
        elif vehicle_class == 'SUV':
            profile_image_dir = "/images/SUV"
            image_name = random.choice(os.listdir(base_url + profile_image_dir))
            profile_image_path = profile_image_dir+"/"+image_name
        elif vehicle_class == 'Sedan':
            profile_image_dir = "/images/SEDAN"
            image_name = random.choice(os.listdir(base_url + profile_image_dir))
            profile_image_path = profile_image_dir+"/"+image_name
        elif vehicle_class == 'Three Wheelers':
            profile_image_dir = "/images/THREE_WHEELER"
            image_name = random.choice(os.listdir(base_url + profile_image_dir))
            profile_image_path = profile_image_dir+"/"+image_name
        elif vehicle_class == 'Tractor':
            profile_image_dir = "/images/TRACTOR"
            image_name = random.choice(os.listdir(base_url + profile_image_dir))
            profile_image_path = profile_image_dir+"/"+image_name
        elif vehicle_class == 'Tractor-Trailer':
            profile_image_dir = "/images/TRACTOR_TRAILER"
            image_name = random.choice(os.listdir(base_url + profile_image_dir))
            profile_image_path = profile_image_dir+"/"+image_name
        
        elif vehicle_class == 'Truck' :
            if axle_count==2:
                profile_image_dir = "/images/2_AXLE_TRUCK"
                image_name = random.choice(os.listdir(base_url + profile_image_dir))
                profile_image_path = profile_image_dir+"/"+image_name
            elif axle_count==3:
                profile_image_dir = "/images/3_AXLE_TRUCK"
                image_name = random.choice(os.listdir(base_url + profile_image_dir))
                profile_image_path = profile_image_dir+"/"+image_name
            elif axle_count==4:
                profile_image_dir = "/images/4_AXLE_TRUCK"
                image_name = random.choice(os.listdir(base_url + profile_image_dir))
                profile_image_path = profile_image_dir+"/"+image_name
            elif axle_count==5:
                profile_image_dir = "/images/5_AXLE_TRUCK"
                image_name = random.choice(os.listdir(base_url + profile_image_dir))
                profile_image_path = profile_image_dir+"/"+image_name


        elif vehicle_class == 'Two wheelers':
            profile_image_dir = "/images/TWO_WHEELER"
            image_name = random.choice(os.listdir(base_url + profile_image_dir))
            profile_image_path = profile_image_dir+"/"+image_name
        elif vehicle_class == 'Van':
            profile_image_dir = "/images/VAN"
            image_name = random.choice(os.listdir(base_url + profile_image_dir))
            profile_image_path = profile_image_dir+"/"+image_name
        elif vehicle_class == 'Bus':
            profile_image_dir = "/images/BUS"
            image_name = random.choice(os.listdir(base_url + profile_image_dir))
            profile_image_path = profile_image_dir+"/"+image_name
        
        print(profile_image_path)
        return profile_image_path

    

            


    
