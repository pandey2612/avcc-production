from resources.common_helper import Helper
import cv2
import time
from threading import Thread, Lock
from multiprocessing import Queue, Value, Process , Manager
import datetime
import uuid
import os
from models.model_query import AVCCModel
import traceback
from .camera_controller import *
from .authenticate import Auth
from ctypes import c_int, c_bool 
from .server import Server
from .response_process import ResponseProcess
import schedule

toll_id = Value

line_percent1 = Value(c_int, 45)
line_percent2 = Value(c_int, 55)

anpr_calibration = Value(c_int , 3)
is_cam_disabled = Value(c_int, 5)

txn_count = Value(c_int, 1)

class AvccAnpr:
    def __init__(self, avcc_queue, anpr_queue, **kwargs):
        self.avcc_queue = avcc_queue
        self.anpr_queue = anpr_queue
        self.cam_update = Value(c_bool, True)
        self.seize = Value(c_bool, False)
        self.processing = Value(c_bool, True)
        self.feed_status = Value(c_bool, True)
        self.credit_points_update = Value(c_bool, False)
        self.is_proccessing_paused = Value(c_bool, False)
        self.anpr_respense_queue = Queue(10)
        self.socket_response_queue = Queue(1)
        self.toll_data = Manager().dict()
        self.toll_data['toll_id'] = 'None'
        self.toll_data['lane_id'] = 'None'
        self.manage_server()
        self.init_config()
        txn_count.value = 1 if AVCCModel.get_today_txn_id(str(datetime.datetime.now()))['today_count']==None else AVCCModel.get_today_txn_id(str(datetime.datetime.now()))['today_count'] +1
        #print(txn_count.value , "Here")
    def manage_server(self):
        self.avcc_server = Server(self.socket_response_queue)
        ip, port = Helper.get_socket_ip_port()
        print(ip, port)
        self.avcc_server.create_server(host_name=ip, port=port)
        self.avcc_server.socket_status = True
        self.avcc_server.start_thread()

    def update_version(self):
        Helper.update_version()

    def pause(self):
        t = Auth.auth_license()
        #print("[info]++++++ ",t)
        self.seize.value = t[0]
        return

        def restart(self):
            self.seize.value = True

        def stop(self):
            self.seize.value = False
            self.processing.value = False

    def init_config(self):
        self.config_thread = Thread(
            target=self.check_config, args=(), daemon=True)
        self.config_thread.start()

    def manage_config(self):
        config_result = AVCCModel.get_config_data()
        
        if len(config_result) > 0:

            try:
                line_percent1 = int(config_result[0]['line_draw_percent1'])
                line_percent2 = int(config_result[0]['line_draw_percent2'])
                cam_controller.avcc_cam_obj.line_params1 = line_percent1
                cam_controller.avcc_cam_obj.line_params2 = line_percent2
                
                """
                is_cam_disabled.value = 0 if (
                    cam_controller.avcc_cam_obj.cam_status == 0 and cam_controller.anpr_cam_obj.cam_status == 0) else 1
                """

            except Exception as e:
                print(e)

            self.toll_data['toll_id'] = config_result[0]['site_name']
            self.toll_data['lane_id'] = config_result[0]['location']
            anpr_calibration.value =  config_result[0]['anpr_calibration']
            #print("ANPR Calibration :: " , anpr_calibration.value)
            #print(self.toll_data)
            


            self.avcc_server.socket_status = True if config_result[0]['socket_status'] == 1 else False

    def memory_management(self):
        Helper.memory_management()
        #txn_count.value = 1 if AVCCModel.get_today_txn_id(str(datetime.datetime.now()))['today_count']==None else AVCCModel.get_today_txn_id(str(datetime.datetime.now()))['today_count'] +1


    def check_config(self):
        
        #schedule.every(32).seconds.do(self.update_version)
        schedule.every(5).minutes.do(self.memory_management)
        while True:
            try:
                self.pause()
                """
                if is_cam_disabled.value == 1:
                    AVCCModel.update_cam_deactivate_status()
                    is_cam_disabled.value == 2

                if is_cam_disabled.value == 0:
                    AVCCModel.update_cam_activate_status()
                    is_cam_disabled.value == 2

                if self.seize.value:
                    AVCCModel.update_active_status()
                """
                self.manage_config()
                time.sleep(10)
                schedule.run_pending()
                
            except:
                continue

    def init_process(self):
        self.avcc_proc = Process(
            target=self.identify_vehicle, args=(), daemon=True)
        self.avcc_proc.start()

    def identify_vehicle(self):
        
        from backend.avcc_s1_updated import AVCC
        today = datetime.date.today()
        today_video = str(datetime.date.today())+"_video"
        video_path = os.path.join(
            os.environ['BASE_IMAGE_PATH'], str(today_video))
        if not os.path.isdir(video_path):
            os.mkdir(video_path)
        
        self.avcc_backend_obj = AVCC(anpr_frame_queue=self.anpr_queue,
                                     anpr_response_queue=self.anpr_respense_queue, gif_write_path=video_path)
        
        while True:
            
            if not self.seize.value:
                print("stopped")
                time.sleep(1)
                continue
            
            try:
                frame = self.avcc_queue.get_nowait()
            except:
                continue

            try:
                anpr_respense = self.anpr_respense_queue.get_nowait()
                #print(str(AVCCModel.get_today_txn_id(anpr_respense['created_datetime'])['today_count']+1))
                if anpr_respense['status'] == True:
                    today = datetime.date.today()
                    image_path = os.path.join(
                        os.environ['BASE_IMAGE_PATH'], str(today))

                    if not os.path.isdir(image_path):
                        os.mkdir(image_path)
                    vehicle_image = os.path.join(
                        str(today), "{}.jpg".format(str(uuid.uuid4())))
                    try:
                        np_image = os.path.join(
                            str(today), "{}.jpg".format(str(uuid.uuid4())))
                        #np_frame_img = cv2.resize(anpr_respense['number_plate_frame'], (200, 100) , interpolation = cv2.INTER_AREA)
                        cv2.imwrite(os.path.join(
                            os.environ['BASE_IMAGE_PATH'], np_image), anpr_respense['number_plate_frame'])
                        np_image = os.path.join("/images", np_image)

                    except Exception as e:
                        #print(e)
                        np_image = 'None'

                    if anpr_respense['ocr'] == None:
                        ocr = 'None'
                    else:
                        ocr = anpr_respense['ocr']
                    #print(anpr_respense['vehicle_front_image'].shape)
                    veh_image_frame = cv2.resize(anpr_respense['vehicle_front_image'], (550, 400) , interpolation = cv2.INTER_AREA)
                    #print(veh_image_frame.shape)
                    try:
                        cv2.imwrite(os.path.join(os.environ['BASE_IMAGE_PATH'], vehicle_image),veh_image_frame  ) 
                    except:
                        pass
                    vehicle_image = os.path.join("/images", vehicle_image)
                    #print("In insert db")
                    #AVCCModel.insert_anpr_vehicle_detect_data('ocr', 'np_image', 'vehicle_image',"anpr_respense['created_datetime']")
                    #AVCCModel.get_avcc_pending_data()
                    try:
                        txn_count_id = str(f'{txn_count.value:05d}')
                        txn_count.value+=1
                        txn_date = anpr_respense['created_datetime'].strftime("%y%m%d")
                        txn_id = txn_date+self.toll_data['toll_id']+self.toll_data['lane_id']+txn_count_id
                    except Exception as e:
                        print(e)
                        txn_id = 'None'
                    #print(txn_count_id)
                    print(txn_id)
                    AVCCModel.insert_anpr_vehicle_detect_data(txn_id , ocr, np_image, vehicle_image, anpr_respense['created_datetime'])

                    try:
                        pending_anpr_data = AVCCModel.get_anpr_pending_data()
                        if self.socket_response_queue.qsize() == 1:
                            dump_data = self.socket_response_queue.get_nowait()
                            self.socket_response_queue.put_nowait([0 , pending_anpr_data])
                        else:
                            self.socket_response_queue.put_nowait([0 , pending_anpr_data])
                    except:
                        pass
                                           
                    #print("In insert db1")
                    
            except Exception as ex:
                #print(ex)
                pass

            try:
                avcc_response = self.avcc_backend_obj.process(
                    True, line_percent1.value, line_percent2.value,anpr_calibration.value, frame  )
                #print(output)
                #print(str(AVCCModel.get_today_txn_id(anpr_respense['created_datetime'])['today_count']+1))
                if avcc_response['avcc_status'] == True:
                    today = datetime.date.today()
                
                    image_path = os.path.join(
                        os.environ['BASE_IMAGE_PATH'], str(today))
                    if not os.path.isdir(image_path):
                        os.mkdir(image_path)

                    merged_image = os.path.join(
                        str(today), "{}.jpg".format(str(uuid.uuid4())))
                    
                    """
                    masked_bw_image = os.path.join(
                        str(today), "{}.jpg".format(str(uuid.uuid4())))
                    masked_rgb_image = os.path.join(
                        str(today), "{}.jpg".format(str(uuid.uuid4())))
                    """
                    
                    #profile_image = os.path.join( image_path, "{}.jpg".format(str(uuid.uuid4())))

                    cv2.imwrite(os.path.join(
                        os.environ['BASE_IMAGE_PATH'], merged_image), avcc_response['merged_frame'])
                    """
                    cv2.imwrite(os.path.join(
                        os.environ['BASE_IMAGE_PATH'], masked_bw_image), avcc_response['mask_bw'])
                    cv2.imwrite(os.path.join(
                        os.environ['BASE_IMAGE_PATH'], masked_rgb_image), avcc_response['mask_rgb'])
                    """
                    

                    merged_image = os.path.join("/images", merged_image)
                    """
                    masked_bw_image = os.path.join("/images", masked_bw_image)
                    masked_rgb_image = os.path.join(
                        "/images", masked_rgb_image)
                    """
                    
                    vehicle_class = 0
                    profile_image = ""
                    """
                    if avcc_response['vehicle_class'] == 'car':
                        vehicle_class = 3
                        if avcc_response['axle_count']==1 or avcc_response['axle_count']==3:
                            avcc_response['axle_count']=2
                        profile_image = Helper.profile_generator(vehicle_class , avcc_response['axle_count'])

                    elif avcc_response['vehicle_class'] == 'bus':
                        vehicle_class = 5
                        if avcc_response['axle_count']==1 or avcc_response['axle_count']==3:
                            avcc_response['axle_count']=2
                        profile_image = Helper.profile_generator(vehicle_class , avcc_response['axle_count'])
                        
                    elif avcc_response['vehicle_class'] == 'lcv' and avcc_response['axle_count'] == 3:
                        vehicle_class = 7
                        profile_image = Helper.profile_generator(vehicle_class , avcc_response['axle_count'])


                    elif avcc_response['vehicle_class'] == 'lcv':
                        vehicle_class = 4
                        

                        if avcc_response['axle_count']==1 :
                            avcc_response['axle_count']=2
                        profile_image = Helper.profile_generator(vehicle_class , avcc_response['axle_count'])

                    elif avcc_response['vehicle_class'] == 'mav':
                        vehicle_class = 8
                        profile_image = Helper.profile_generator(vehicle_class , avcc_response['axle_count'])
                    elif avcc_response['vehicle_class'] == 'truck':
                        if avcc_response['axle_count'] == 2:
                            vehicle_class = 6
                            profile_image = Helper.profile_generator(vehicle_class , avcc_response['axle_count'])

                        elif avcc_response['axle_count'] == 3:
                            vehicle_class = 7
                            profile_image = Helper.profile_generator(vehicle_class , avcc_response['axle_count'])

                        elif avcc_response['axle_count'] > 3:
                            vehicle_class = 8
                            profile_image = Helper.profile_generator(vehicle_class , avcc_response['axle_count'])

                        else:
                            vehicle_class = 0
                            profile_image = Helper.profile_generator(10 , avcc_response['axle_count'])
                    """
                    
                    
                    if avcc_response['vehicle_class'] == 'No_Class':
                        vehicle_class = 0
                        profile_image = 'None'
                    elif avcc_response['vehicle_class'] == 'Hatchback' or avcc_response['vehicle_class'] == 'Jeep' or avcc_response['vehicle_class'] == 'SUV' or avcc_response['vehicle_class'] == 'Sedan' or avcc_response['vehicle_class'] == 'Van':
                        vehicle_class = 3
                        if avcc_response['axle_count']==1 or avcc_response['axle_count']==3:
                            avcc_response['axle_count']=2
                        profile_image = Helper.profile_generator(avcc_response['vehicle_class'] , avcc_response['axle_count'])
                    
                    elif avcc_response['vehicle_class'] == 'LCV' or avcc_response['vehicle_class'] == 'Mini Truck' or avcc_response['vehicle_class'] == 'Pickup Truck':
                        vehicle_class = 4
                        if avcc_response['axle_count']==1  or  avcc_response['axle_count']==3 :
                            avcc_response['axle_count']=2
                        
                        profile_image = Helper.profile_generator(avcc_response['vehicle_class'] , avcc_response['axle_count'])
                    
                    elif avcc_response['vehicle_class'] == 'MAV':
                        if avcc_response['axle_count'] == 1:
                            avcc_response['axle_count'] == 2 
                        if avcc_response['axle_count'] == 2:
                            vehicle_class = 6
                            profile_image = Helper.profile_generator('Truck', avcc_response['axle_count'])

                        elif avcc_response['axle_count'] == 3:
                            vehicle_class = 7
                            profile_image = Helper.profile_generator('Truck' , avcc_response['axle_count'])

                        else:
                            vehicle_class = 8
                            profile_image = Helper.profile_generator(avcc_response['vehicle_class'] , avcc_response['axle_count'])

                    elif avcc_response['vehicle_class'] == 'Mini Bus' or avcc_response['vehicle_class'] == 'Bus':
                        vehicle_class = 5
                        if avcc_response['axle_count']==1 or avcc_response['axle_count']==3:
                            avcc_response['axle_count']=2
                        profile_image = Helper.profile_generator(avcc_response['vehicle_class'] , avcc_response['axle_count'])
                        
                    elif avcc_response['vehicle_class'] == 'Three Wheelers':
                        vehicle_class = 2
                        profile_image = Helper.profile_generator(avcc_response['vehicle_class'] , avcc_response['axle_count'])


                    elif avcc_response['vehicle_class'] == 'Tractor':
                        vehicle_class = 10
                        if avcc_response['axle_count'] == 1:
                            avcc_response['axle_count'] == 2
                        profile_image = Helper.profile_generator(avcc_response['vehicle_class'] , avcc_response['axle_count'])

                    elif avcc_response['vehicle_class'] == 'Tractor-Trailer':
                        vehicle_class = 11
                        if avcc_response['axle_count'] == 1:
                            avcc_response['axle_count'] == 2
                        profile_image = Helper.profile_generator(avcc_response['vehicle_class'] , avcc_response['axle_count'])

                    elif avcc_response['vehicle_class'] == 'Truck':
                        if avcc_response['axle_count'] == 1:
                            avcc_response['axle_count'] == 2

                        if avcc_response['axle_count'] == 2:
                            vehicle_class = 6
                            profile_image = Helper.profile_generator(avcc_response['vehicle_class'] , avcc_response['axle_count'])

                        elif avcc_response['axle_count'] == 3:
                            vehicle_class = 7
                            profile_image = Helper.profile_generator(avcc_response['vehicle_class'] , avcc_response['axle_count'])

                        elif avcc_response['axle_count'] > 3:
                            vehicle_class = 8
                            profile_image = Helper.profile_generator(avcc_response['vehicle_class'] , avcc_response['axle_count'])

                    elif avcc_response['vehicle_class'] == 'Two wheelers':
                        vehicle_class = 1
                        profile_image = Helper.profile_generator(avcc_response['vehicle_class'] , avcc_response['axle_count'])

                    
                    
                    
                    record_id = AVCCModel.get_anpr_record_id()
                    if record_id:
                        
                        
                        #AVCCModel.insert_vehicle_detect_data(avcc_response['axle_count'] , ocr , vehicle_class ,   vehicle_image , np_image , masked_rgb_image , masked_bw_image , merged_image ,  '/images/'+self.today_video+'/'+ avcc_response['video_path'], str(avcc_response['time']))
                        AVCCModel.update_avcc_vehicle_detect_data(avcc_response['axle_count'], vehicle_class, 'None', 'None',merged_image, '/images/'+today_video+'/' + avcc_response['video_path'],  str(avcc_response['time']), record_id[0]['id'] , profile_image)
                        try:
                            pending_avcc_data = AVCCModel.get_avcc_pending_data()
                            if self.socket_response_queue.qsize() == 1:
                                dump_data = self.socket_response_queue.get_nowait()
                                self.socket_response_queue.put_nowait([1, pending_avcc_data])
                            else:
                                self.socket_response_queue.put_nowait([1 , pending_avcc_data])
                        except Exception as e:
                            #print(e)
                            #print("I")
                            pass
                    else:
                    
                        #print("in getting rec ")
                        #AVCCModel.insert_avcc_vehicle_detect_data('axle_count','vehicle_class' , 'masked_rgb_image', 'masked_bw_image' , 'merged_image'  , 'video_path' , 'avcc_created_datetime' )
                        try:
                            txn_count_id =  str(f'{txn_count.value:05d}')
                            txn_count.value+=1
                            txn_date = avcc_response['time'].strftime("%y%m%d")
                            txn_id = txn_date+self.toll_data['toll_id']+self.toll_data['lane_id']+txn_count_id

                        except Exception as e:
                            print(e)
                            txn_id = 'None' 
                        
                        #print("IN ELSE WHILE INSERTING AVCC :: " , txn_id)
                        try:
                            AVCCModel.insert_anpr_vehicle_detect_data( txn_id, 'None', 'None', 'None', 'None')
                            record_id = AVCCModel.get_anpr_record_id()
                            AVCCModel.update_avcc_vehicle_detect_data(avcc_response['axle_count'], vehicle_class, 'None', 'None',merged_image, '/images/'+today_video+'/' + avcc_response['video_path'],  str(avcc_response['time']), record_id[0]['id'] , profile_image)
                            print(txn_id)
                            #AVCCModel.insert_avcc_vehicle_detect_data(avcc_response['axlecount'], vehicle_class, masked_rgb_image, masked_bw_image, merged_image, '/images/'+self.today_video+'/' + avcc_response['video_path'],  str(avcc_response['time']) , profile_image)
                            try:
                                pending_avcc_data = AVCCModel.get_avcc_pending_data()
                                #print(pending_avcc_data)
                                #print(self.socket_response_queue.qsize())
                                if self.socket_response_queue.qsize() == 1:
                                    #print("in avcc queue insertion")
                                    #print(pending_avcc_data)
                                    dump_data = self.socket_response_queue.get_nowait()
                                    #print(dump_data)
                                    #print(self.socket_response_queue.qsize())  
                                    self.socket_response_queue.put_nowait([1, pending_avcc_data])
                                    #print("inserted in queue")
                                else:
                                    #print("in else adding")
                                    #print(self.socket_response_queue.qsize())
                                    self.socket_response_queue.put_nowait([1 , pending_avcc_data])
                                    #print("In else added")
                            except:
                                pass


                            
                        except Exception as e:
                            #print(e)
                            pass
                            
                        #print("TESTING")
                    today_video = str(datetime.date.today())+"_video"
                    video_path = os.path.join(
                        os.environ['BASE_IMAGE_PATH'], str(today_video))
                    if not os.path.isdir(video_path):
                        os.mkdir(video_path)
                        self.avcc_backend_obj.gif_write_path = video_path
            except Exception as avccRespProcExce:
                print(avccRespProcExce)
                traceback.print_exc()


        '''
        today = datetime.date.today()
        today_video = str(datetime.date.today())+"_video"
        video_path = os.path.join(
            os.environ['BASE_IMAGE_PATH'], str(today_video))
        if not os.path.isdir(video_path):
            os.mkdir(video_path)
        
        

        self.avcc_backend_obj = AVCC(anpr_frame_queue=self.anpr_queue,
                                     anpr_response_queue=self.anpr_respense_queue, gif_write_path=video_path)
        self.avcc_response_obj = ResponseProcess(self.anpr_respense_queue , txn_count , self.socket_response_queue , self.toll_data , self.avcc_backend_obj  )

        while True:
            
            if not self.seize.value:
                print("stopped")
                time.sleep(1)
                continue
            
            try:
                frame = self.avcc_queue.get_nowait()
            except:
                continue
            """
            try:
                anpr_respense = self.anpr_respense_queue.get_nowait()

                #print(str(AVCCModel.get_today_txn_id(anpr_respense['created_datetime'])['today_count']+1))
                if anpr_respense['status'] == True:
                    today = datetime.date.today()
                    image_path = os.path.join(
                        os.environ['BASE_IMAGE_PATH'], str(today))

                    if not os.path.isdir(image_path):
                        os.mkdir(image_path)
                    vehicle_image = os.path.join(
                        str(today), "{}.jpg".format(str(uuid.uuid4())))
                    try:
                        np_image = os.path.join(
                            str(today), "{}.jpg".format(str(uuid.uuid4())))
                        #np_frame_img = cv2.resize(anpr_respense['number_plate_frame'], (200, 100) , interpolation = cv2.INTER_AREA)
                        cv2.imwrite(os.path.join(
                            os.environ['BASE_IMAGE_PATH'], np_image), anpr_respense['number_plate_frame'])
                        np_image = os.path.join("/images", np_image)

                    except Exception as e:
                        #print(e)
                        np_image = 'None'

                    if anpr_respense['ocr'] == None:
                        ocr = 'None'
                    else:
                        ocr = anpr_respense['ocr']
                    #print(anpr_respense['vehicle_front_image'].shape)
                    veh_image_frame = cv2.resize(anpr_respense['vehicle_front_image'], (550, 400) , interpolation = cv2.INTER_AREA)
                    #print(veh_image_frame.shape)
                    try:
                        cv2.imwrite(os.path.join(os.environ['BASE_IMAGE_PATH'], vehicle_image),veh_image_frame  ) 
                    except:
                        pass
                    vehicle_image = os.path.join("/images", vehicle_image)
                    #print("In insert db")
                    #AVCCModel.insert_anpr_vehicle_detect_data('ocr', 'np_image', 'vehicle_image',"anpr_respense['created_datetime']")
                    #AVCCModel.get_avcc_pending_data()
                    try:
                        txn_count_id = str(f'{txn_count.value:05d}')
                        txn_count.value+=1
                        txn_date = anpr_respense['created_datetime'].strftime("%y%m%d")
                        txn_id = txn_date+self.toll_data['toll_id']+self.toll_data['lane_id']+txn_count_id
                    except Exception as e:
                        print(e)
                        txn_id = 'None'
                    #print(txn_count_id)
                    print(txn_id)
                    AVCCModel.insert_anpr_vehicle_detect_data(txn_id , ocr, np_image, vehicle_image, anpr_respense['created_datetime'])

                    try:
                        pending_anpr_data = AVCCModel.get_anpr_pending_data()
                        if self.socket_response_queue.qsize() == 1:
                            dump_data = self.socket_response_queue.get_nowait()
                            self.socket_response_queue.put_nowait([0 , pending_anpr_data])
                        else:
                            self.socket_response_queue.put_nowait([0 , pending_anpr_data])
                    except:
                        pass
                                           
                    #print("In insert db1")
                    
            except Exception as ex:
                #print(ex)
                pass
            """
            try:
                output = self.avcc_backend_obj.process(
                    True, line_percent1.value, line_percent2.value,anpr_calibration.value, frame  )

                
                #print(output)
                #print(str(AVCCModel.get_today_txn_id(anpr_respense['created_datetime'])['today_count']+1))
                if output['avcc_status'] == True:
                    self.anpr_respense_queue.put_nowait([1,output])
                    """
                    today = datetime.date.today()
                    image_path = os.path.join(
                        os.environ['BASE_IMAGE_PATH'], str(today))
                    if not os.path.isdir(image_path):
                        os.mkdir(image_path)

                    merged_image = os.path.join(
                        str(today), "{}.jpg".format(str(uuid.uuid4())))
                    masked_bw_image = os.path.join(
                        str(today), "{}.jpg".format(str(uuid.uuid4())))
                    masked_rgb_image = os.path.join(
                        str(today), "{}.jpg".format(str(uuid.uuid4())))
                    #profile_image = os.path.join( image_path, "{}.jpg".format(str(uuid.uuid4())))

                    cv2.imwrite(os.path.join(
                        os.environ['BASE_IMAGE_PATH'], merged_image), output['merged_frame'])
                    cv2.imwrite(os.path.join(
                        os.environ['BASE_IMAGE_PATH'], masked_bw_image), output['mask_bw'])
                    cv2.imwrite(os.path.join(
                        os.environ['BASE_IMAGE_PATH'], masked_rgb_image), output['mask_rgb'])

                    merged_image = os.path.join("/images", merged_image)
                    masked_bw_image = os.path.join("/images", masked_bw_image)
                    masked_rgb_image = os.path.join(
                        "/images", masked_rgb_image)
                    vehicle_class = 0
                    if output['vehicle_class'] == 'car':

                        vehicle_class = 3
                        if output['axle_count']==1 or output['axle_count']==3:
                            output['axle_count']=2
                    elif output['vehicle_class'] == 'bus':
                        vehicle_class = 5
                        if output['axle_count']==1 or output['axle_count']==3:
                            output['axle_count']=2
                    elif output['vehicle_class'] == 'lcv' and output['axle_count'] == 3:
                        vehicle_class = 7

                    elif output['vehicle_class'] == 'lcv':
                        vehicle_class = 4
                        if output['axle_count']==1 :
                            output['axle_count']=2
                    elif output['vehicle_class'] == 'mav':
                        vehicle_class = 8
                    elif output['vehicle_class'] == 'truck':
                        if output['axle_count'] == 2:
                            vehicle_class = 6

                        elif output['axle_count'] == 3:
                            vehicle_class = 7
                        elif output['axle_count'] > 3:
                            vehicle_class = 8

                        else:
                            vehicle_class = 0
                    
                    
                    record_id = AVCCModel.get_anpr_record_id()
                    if record_id:
                        
                        
                        #AVCCModel.insert_vehicle_detect_data(output['axle_count'] , ocr , vehicle_class ,   vehicle_image , np_image , masked_rgb_image , masked_bw_image , merged_image ,  '/images/'+today_video+'/'+ output['video_path'], str(output['time']))
                        AVCCModel.update_avcc_vehicle_detect_data(output['axle_count'], vehicle_class, masked_rgb_image, masked_bw_image,merged_image, '/images/'+today_video+'/' + output['video_path'],  str(output['time']), record_id[0]['id'])
                        try:
                            pending_avcc_data = AVCCModel.get_avcc_pending_data()
                            if self.socket_response_queue.qsize() == 1:
                                dump_data = self.socket_response_queue.get_nowait()
                                self.socket_response_queue.put_nowait([1, pending_avcc_data])
                            else:
                                self.socket_response_queue.put_nowait([1 , pending_avcc_data])
                        except Exception as e:
                            #print(e)
                            #print("I")
                            pass

                        
                    else:
                    
                        #print("in getting rec ")
                        #AVCCModel.insert_avcc_vehicle_detect_data('axle_count','vehicle_class' , 'masked_rgb_image', 'masked_bw_image' , 'merged_image'  , 'video_path' , 'avcc_created_datetime' )
                        try:
                            txn_count_id =  str(f'{txn_count.value:05d}')
                            txn_count.value+=1
                            txn_date = anpr_respense['created_datetime'].strftime("%y%m%d")
                            txn_id = txn_date+self.toll_data['toll_id']+self.toll_data['lane_id']+txn_count_id

                        except Exception as e:
                            print(e)
                            txn_id = 'None'
                        try:
                            #AVCCModel.insert_anpr_vehicle_detect_data( txn_id, 'None', 'None', 'None', 'None')
                            #record_id = AVCCModel.get_anpr_record_id()
                            #AVCCModel.update_avcc_vehicle_detect_data(output['axle_count'], vehicle_class, masked_rgb_image, masked_bw_image,merged_image, '/images/'+today_video+'/' + output['video_path'],  str(output['time']), record_id[0]['id'])
                            print(txn_id)
                            AVCCModel.insert_avcc_vehicle_detect_data(output['axle_count'], vehicle_class, masked_rgb_image, masked_bw_image, merged_image, '/images/'+today_video+'/' + output['video_path'],  str(output['time']))
                            try:
                                pending_avcc_data = AVCCModel.get_avcc_pending_data()
                                #print(pending_avcc_data)
                                #print(self.socket_response_queue.qsize())
                                if self.socket_response_queue.qsize() == 1:
                                    #print("in avcc queue insertion")
                                    #print(pending_avcc_data)
                                    dump_data = self.socket_response_queue.get_nowait()
                                    #print(dump_data)
                                    #print(self.socket_response_queue.qsize())
                                    self.socket_response_queue.put_nowait([1, pending_avcc_data])
                                    #print("inserted in queue")
                                else:
                                    #print("in else adding")
                                    #print(self.socket_response_queue.qsize())
                                    self.socket_response_queue.put_nowait([1 , pending_avcc_data])
                                    #print("In else added")
                            except:
                                pass


                            
                        except Exception as e:
                            #print(e)
                            pass
                            
                        #print("TESTING")
                    today_video = str(datetime.date.today())+"_video"
                    video_path = os.path.join(
                        os.environ['BASE_IMAGE_PATH'], str(today_video))
                    if not os.path.isdir(video_path):
                        os.mkdir(video_path)
                        self.avcc_backend_obj.gif_write_path = video_path
                """

            except Exception as out_exc:
                traceback.print_exc()
                print(out_exc)
        
        '''

