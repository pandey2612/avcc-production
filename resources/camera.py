from os import stat
import cv2
import time
from threading import Thread ,Lock
from multiprocessing import Queue , Value ,Process
import os


class CameraFeed:
    def __init__(self ,proc_queue, src=0  , resolution=(1280 , 720) , cam_type=0   ):
        try:
            self.lock = Lock()
            self.src = src 
            self.cam_type = cam_type
            self.outputFrame = None
            self.proc_queue = proc_queue
            self.line_params1 = 45
            self.line_params2 = 55
            
            '''print("current Threads :: " , cv2.getNumThreads())
            cv2.setNumThreads(1)
            print("Post Threads :: " , cv2.getNumThreads())'''
            self.capture_source = cv2.VideoCapture(self.src) 
            #self.capture_source.set(5,10)
            self.cam_status=5
            self.start_cam_thread()

        except Exception as ex:
            print(ex)

    def start_cam_thread(self):
        self.cam_thread=Thread(target=self.update_camera,args=(),daemon=True) 
        self.cam_thread.start()

    def stop_camera(self):
        self.start_cam_thread()
        self.capture_source.release()
        
    def stop_cam_thread(self):
        self.cam_thread.join()

    def update_camera(self):
        #os.sched_setaffinity(process_id, {0}) #to run the process on 1st core / to run on more than one core = {0,1}
        #print(os.sched_getaffinity(process_id))
        while True:
            try:
                status , frame  = self.capture_source.read()
                
                if self.cam_status !=2:
                    self.cam_status = 0
                if status:
                    try:
                        self.proc_queue.put_nowait(frame.copy())
                    except :
                        pass
                    
                    with self.lock:
                        if self.cam_type == 0:
                            stream_frame = self.draw_boxes(cv2.resize(frame.copy(),(1280,720)) , self.line_params1 , self.line_params2)
                        else:
                            stream_frame = cv2.resize(frame.copy(),(1280,720))

                        self.outputFrame = stream_frame  #bframe.copy() 
                else:
                    self.cam_status=1
                    #if not self.capture_source.isOpened():
                    print('Here')
                    self.capture_source.release()
                    self.capture_source = cv2.VideoCapture(self.src)
                    time.sleep(2)
            except Exception as e:
                print(e)
                self.cam_status=1
                if not self.capture_source.isOpened():
                    try:
                        self.capture_source.release()
                        self.capture_source = cv2.VideoCapture(self.src)
                        continue
                    except Exception as ex:
                        #self.capture_source = cv2.VideoCapture(self.src)
                        continue
                continue


    
    def generate(self):
        while True:
            try:
                
                with self.lock:
                    if self.outputFrame is None:
                        continue
                
                (flag, encodedImage) = cv2.imencode(".jpg", self.outputFrame )
                frameret = encodedImage.tobytes()
                if not flag:
                    continue
            
                yield(b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' + 
                    bytearray(encodedImage) + b'\r\n') #bytearray(encodedImage)
            except:
                continue


    def draw_boxes(self,image, line_prcnt1 , line_prcnt2):
        H,W = image.shape[:2]
        #orig_frame = f.copy()
        p1 = (0,int(W*line_prcnt1/100))
        p2 = (H, int(W*line_prcnt1/100))
        p3 = (0,int(W*line_prcnt2/100))
        p4 = (H, int(W*line_prcnt2/100))
        cv2.line(image,(int(line_prcnt1/100*W),0),(int(line_prcnt1/100*W),H),(0,0,255),1)
        cv2.line(image,(int(line_prcnt2/100*W),0),(int(line_prcnt2/100*W),H),(0,255,0),1)

        return image

