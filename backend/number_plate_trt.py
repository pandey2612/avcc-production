import cv2
import pycuda.autoinit
from .yolo_with_plugins_np import TrtYOLO
from . import ocr_trt
import os
class Number_Plate :
    def __init__(self) :
        self.np_model = os.path.join(os.getcwd(),"files","number-plate-yolov3-tiny-384.trt")
        self.np_trt_yolo = TrtYOLO(self.np_model, (384,384), 1)
        self.confidence_threshold = 0.25
        self.ocr_cls = ocr_trt.OCR()


    def detection(self,frame) :
        info = []
        #cv2.imshow("n", frame)
        #cv2.waitKey()
        boxes, confs, clss = self.np_trt_yolo.detect(frame, self.confidence_threshold)
        for bb, cf, cl in zip(boxes, confs, clss):
            cl = int(cl)
            x_min, y_min, x_max, y_max = bb[0], bb[1], bb[2], bb[3]
            #cv2.rectangle(frame, (x_min, y_min), (x_max, y_max), (0,0,255), 2)
            np_roi = frame[y_min:y_max, x_min:x_max]
            ocr_text = self.ocr_cls.detection(np_roi)
            
            #info.append([ocr_text,[x_min,y_min,x_max-x_min,y_max-y_min]])
            info.append([x_min,y_min,x_max-x_min,y_max-y_min, ocr_text, cf])
            

        return info
