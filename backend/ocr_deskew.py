import numpy as np
import cv2
from glob import glob


def rotate(point, origin, degrees):
    radians = np.deg2rad(degrees)
    x,y = point
    offset_x, offset_y = origin
    adjusted_x = (x - offset_x)
    adjusted_y = (y - offset_y)
    cos_rad = np.cos(radians)
    sin_rad = np.sin(radians)
    qx = offset_x + cos_rad * adjusted_x + sin_rad * adjusted_y
    qy = offset_y + -sin_rad * adjusted_x + cos_rad * adjusted_y
    return int(qx), int(qy)


def deskew_plate(image,txt_data):
        
        #print(image.shape)
        #image = cv2.imread(path)
        #show_image=image.copy()
        img_width,img_height=image.shape[1],image.shape[0]
        #txt_data=np.loadtxt(path[:-4]+'.txt')
        bg=np.zeros_like(image)

        if len(txt_data.shape)<2:
                txt_data=np.expand_dims(txt_data,axis=0)

        for data in txt_data:
        
                class_,conf,xmin,ymin,w,h=data
                xmin,ymin,xmax,ymax=int(xmin),int(ymin),int(xmin+w),int(ymin+h)
                #cv2.rectangle(image,(xmin,ymin),(xmax,ymax),(0,0,255),2)
                #print(xmin,ymin,xmax,ymax)
                bg[ymin:ymax,xmin:xmax]=image[ymin:ymax,xmin:xmax]
        
        
        
        gray = cv2.cvtColor(bg, cv2.COLOR_BGR2GRAY)
        #gray=bg.copy()
        #gray=cv2.bitwise_not(gray)

        
        thresh = cv2.threshold(gray, 0, 255,
	cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

        #cv2.imshow('bg',bg)
        #cv2.imshow('thresh',thresh)
        coords = np.column_stack(np.where(thresh > 0))

        (_,_,angle) = cv2.minAreaRect(coords)
        
        #print(angle)

        if angle < -45:
                angle = -(90 + angle)

        else:
                angle = -angle

        #print('angle:',angle)
        (h, w) = image.shape[:2]
        center = (w // 2, h // 2)
        #M = cv2.getRotationMatrix2D(center, angle, 1.0)
        #rotated = cv2.warpAffine(image, M, (w, h),
        #        flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)

        

        rotated_data=[]

        for data in txt_data:
        
                class_,conf,xmin,ymin,w,h=data
                xmin,ymin,xmax,ymax=int(xmin),int(ymin),int(xmin+w),int(ymin+h)
                
                rotated_xmin,rotated_ymin=rotate((xmin,ymin),center, angle)
                rotated_xmax,rotated_ymax=rotate((xmax,ymax),center, angle)
                
                #cv2.rectangle(rotated,(rotated_xmin,rotated_ymin),(rotated_xmax,rotated_ymax),(0,0,255),2)
                rotated_data.append([rotated_xmin,rotated_ymin,rotated_xmax,rotated_ymax])
        #cv2.imshow('rotated',rotated)        
        return np.array(rotated_data)
