'''
Copyright 2021 Vignesh Kotteeswaran <Intozi Tech Pself Ltd,vignesh@intozi.io>
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''

import cv2
import traceback
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf

gpus = tf.config.list_physical_devices('GPU')
if gpus:
  try:
    # Currently, memory growth needs to be the same across GPUs
    for gpu in gpus:
      #tf.config.experimental.set_memory_growth(gpu, True)
      tf.config.set_logical_device_configuration(
      gpu,
      [tf.config.LogicalDeviceConfiguration(memory_limit=128)])
      
    logical_gpus = tf.config.list_logical_devices('GPU')
    print('\n')
    print('***** Limiting GPU Growth *****')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    print('\n')
  except RuntimeError as e:
    # Memory growth must be set before GPUs have been initialized
    print(e)

from .centroidtracker import CentroidTracker
from .detector_and_multiclass_vehicle_segmentor_trt import Detector_and_MultiClass_VehicleSegmentor
from tensorflow.keras import backend as K
import numpy as np
from tensorflow.keras.preprocessing.image import img_to_array,load_img
from matplotlib import pyplot as plt
from time import time
import pandas as pd
import os
from glob import glob
from datetime import datetime
from shutil import copy
from .ocr_trt import OCR
from .number_plate_trt import Number_Plate
import shutil
from PIL import Image

### remove this in production ###
from random import choice

### remove this in production ###

#managed by Backend
#if not(os.path.exists('videos')):
#  os.mkdir('videos')

#managed by Backend
#if not(os.path.exists('mask')):
#  os.mkdir('mask')


#managed by Backend
#if not(os.path.exists('merged')):
#  os.mkdir('merged')


class AVCC():

  def __init__(self,anpr_frame_queue=None , anpr_response_queue = 'None',write_gif=True , gif_write_path = '/var/www/html/videos/' ):

    '''
    Please check for folder 'gifs' , 'gif_frames'
    '''
    '''
    self.temp_file_path = os.getcwd()+'/backend/frames'
    
    if not(os.path.exists(self.temp_file_path)):
      os.mkdir(self.temp_file_path)

    else:
      os.system('rm -r '+self.temp_file_path+'/*')
    os.system("sudo chmod 777 "+ self.temp_file_path)
      #shutil.rmtree(self.temp_file_path+'/')
    
    
    self.temp_gif_path = os.getcwd()+'/backend/gif_frames'
    
    if not(os.path.exists(self.temp_gif_path)):
      os.mkdir(self.temp_gif_path)

    else:
      os.system('rm -r '+self.temp_gif_path+'/*')
      #shutil.rmtree(self.temp_file_path+'/')
      
    os.system("sudo chmod 777 "+ self.temp_gif_path)
    '''

    start=time()
    
    self.img_width=384
    self.img_height=256
    self.gif_width=192
    self.gif_height=96
    self.num_gif_frames_limit=30
    self.curr_axle_id=-1
    self.curr_type=[]
    self.axle_ct = CentroidTracker(0)
    self.write_light=False
    self.axle_ids=[]
    self.written_ids=[]
    self.write_gif=write_gif
    self.anpr_frame_queue = anpr_frame_queue
    self.ocr=None
    self.number_plate_frame=None
    
    self.info={}
    self.gif_write_path = gif_write_path
    self.anpr_response_queue = anpr_response_queue
    self.info['ocr_frame_required']=False
    self.info['avcc_status']=False
    
    self.ocr_frame=None
    self.gif_frames=[]
    self.merge_frames={}
    K.clear_session()
    #self.detect_and_segment=Detector_and_Segmentor(modelpath='axle_front_back20.h5',maskmodelpath='vehicle_axle_segment6.h5',size=(self.img_width,self.img_height))
    self.detect_and_segment=Detector_and_MultiClass_VehicleSegmentor(modelpath=os.getcwd()+'/files/'+'axle_front_back_others_32_mAP90.0_axleAP90.26_backAP88.17_nano2gb',maskmodelpath=os.getcwd()+'/files/'+'vehicle_axle_segment13_nano_trt',
        img_width=self.img_width,img_height=self.img_height)
    
    self.number_plate_model=Number_Plate()
    print('Number Plate model loaded')
    self.ocr_model=OCR()
    print('OCR model loaded')
    end=time()
    print('Time Elapsed for Init & Warmup:',end-start)
   
    self.avcc_frame_counter=0
    self.avcc_start_time=time()
    '''
    Line values
    cv2.line(show_frame,(int(0.5*frame.shape[1]),0),(int(0.5*frame.shape[1]),frame.shape[0]),(255,0,0),1)
    cv2.line(show_frame,(int(0.65*frame.shape[1]),0),(int(0.65*frame.shape[1]),frame.shape[0]),(255,0,0),1)'''
    

  def merge_and_clear_frames(self,merge_frames):
    
    if len(merge_frames)<1:
        return

    frames=[]
    frame_widths=[]
    for key_ in sorted(merge_frames.keys(),reverse=True):
      #print('merge key:',key_)
      img=merge_frames[key_]
      frame_height=img.shape[0]
      frame_width=img.shape[1]
      frames.append(img)
      frame_widths.append(frame_width)

    merged_frame=np.zeros((frame_height,sum(frame_widths),3))
    start_width=0
    for img,width in zip(frames,frame_widths):
      end_width=start_width+width
      merged_frame[:,start_width:end_width]=img
      start_width=end_width
      
    return merged_frame

  '''
  def create_gif(self):

    frames = []
    
    #files = sorted(glob(self.temp_gif_path+"/*.jpg"))
    #files.sort(key=os.path.getmtime)
    num_files=len(files)
    if num_files>20:
      frame_interval=num_files//20
    else:
      frame_interval=1
    
    index=0
    for filename in (files):
      if index%frame_interval==0:
        #print(index,filename,frame_interval)
        new_frame = Image.open(filename)
        new_frame = new_frame.resize((240,180), Image.ANTIALIAS)
        frames.append(new_frame)
      index+=1
    os.system('rm -r '+self.temp_gif_path+'/*')
    return frames
  '''
  def do_ocr(self,ocr_frame):
    #pass
    
    number_plate_infos=self.number_plate_model.detection(ocr_frame)
    #print('number_plate_info:',number_plate_infos)
    for info in number_plate_infos:
      x,y,w,h,text,conf=info
      self.number_plate_frame=ocr_frame[y:y+h,x:x+w]
      self.ocr=text#self.ocr_model.detection(self.number_plate_frame)

    self.info['ocr_frame_required']=False

    print('[INFO]: OCR DONE & Disableds OCR Required')
    
  '''
  entering the brain
  '''
  def calc_fps(self):
    return self.avcc_frame_counter//(time()-self.avcc_start_time)

  def process(self,ret,  line_param1 , line_param2, anpr_calibration, frame=None):
  
    '''
    Args:
    ret: Boolean returned by OpenCV video reader
    frame: numpy array of image
    ''' 
    #print(frame.shape)
    try:
        self.ocr_frame = self.anpr_frame_queue.get_nowait()
    except:
        pass
    if self.info['ocr_frame_required']:
      try:
        #temp_frame=self.anpr_frame_queue.get_nowait()
        
        start_time=time()
        self.do_ocr(self.ocr_frame)
        end_time=time()
        #print('\n')
        print('Time Taken for OCR:',end_time-start_time)
        info = {}
        if self.ocr == None:
          self.ocr="None"
        #info['ocr_frame_required']=False
        info['status'] = True
        info['vehicle_front_image']=self.ocr_frame
        info['ocr']=self.ocr
        info['number_plate_frame']=self.number_plate_frame
        info['created_datetime'] = datetime.now()
        #cv2.imwrite(str(datetime.now())+"_"+self.ocr+".jpg" , self.ocr_frame)
        try:
          self.anpr_response_queue.put_nowait([0,info])
        except Exception as anpr_nowait_failed1:
          print('[ERROR]:anpr_nowait_failed1 ',anpr_nowait_failed1)
          pass

        #print('********** OCR Done *********')
        #print('\n')
      except Exception as failed:
        print(failed)
        #cv2.imwrite(str(datetime.now())+"_failed_"+self.ocr+".jpg" , self.ocr_frame)
        self.ocr_frame = frame
        info = {}
        info['status'] = True
        info['vehicle_front_image']=None#self.ocr_frame
        info['ocr']='None'
        info['number_plate_frame']=None#self.number_plate_frame
        info['created_datetime'] = datetime.now()
        try:
          self.anpr_response_queue.put_nowait([0,info])
        except Exception as anpr_nowait_failed2:
          print('[ERROR]:anpr_nowait_failed2 ',anpr_nowait_failed2)
          pass

        
      
      
    self.info={}
    self.info['ocr_frame_required']=False
    #print('[INFO]: Disable OCR Required')
    self.info['avcc_status']=False

    if ret == True:
      #process_start_time=time()
      frame=cv2.resize(frame,(int(384*1.5),int(384*1.5)))
      frame_height,frame_width=frame.shape[1],frame.shape[0]
    
      blob=cv2.cvtColor(cv2.resize(frame,(self.img_height,self.img_width)),cv2.COLOR_BGR2RGB).astype(np.float32)
      #start_time=time()
      dpred=self.detect_and_segment.detect(blob,filter_=False,filter_thresh=0.1,iou_threshold=0.1,confidence_thresh=0.95)
      #end_time=time()
      #print('stage1 inference time:',end_time-start_time)
      axle_rects=[]     # collects list of axle rectangle bounding box in the image
      if hasattr(dpred,'shape'): ## check if detections are not empty
        for class_,score,coords in zip(dpred[:,0],dpred[:,1],dpred[:,-4:]):
          coords=[int((coords[0]/self.img_width)*frame_width),int((coords[1]/self.img_height)*frame_height),int((coords[2]/self.img_width)*frame_width),int((coords[3]/self.img_height)*frame_height)]
          xmin,ymin,xmax,ymax=[i for i in coords]
          centroid=[int((xmin+xmax)/2),int((ymin+ymax)/2)]
        
          if class_!=6:
            #text=self.classes[int(class_)-1]
            if class_<5 and centroid[0]<=int(0.65*frame.shape[1]):

              if self.write_gif:
                  self.gif_frames.append(Image.fromarray(cv2.resize(cv2.cvtColor(frame,cv2.COLOR_BGR2RGB),(self.gif_width,self.gif_height))))

              self.curr_type.append(class_)
              self.axle_ids=[]
              self.vehicle_front_image=frame
              #print('{} times vehicle front found'.format(str(len(self.curr_type)))) #remove in stable
              if len(self.curr_type)<3:
              
                self.axle_ct = CentroidTracker(0)

              if len(self.curr_type)==anpr_calibration:# earlier detection for ANPR default is 2
                  print("INFO :: OCR Required " , anpr_calibration) # erase 
                  
                  self.info['ocr_frame_required']=True
                
          
            if class_==5 and centroid[0]>=int(0.65*frame.shape[1]):

              #print('[INFO]: VEHICLE BACK FOUND') #remove in stable
            
              if len(self.curr_type)>0 and len(self.axle_ids)>0:

                if self.write_gif:
                  self.gif_frames.append(Image.fromarray(cv2.resize(cv2.cvtColor(frame,cv2.COLOR_BGR2RGB),(self.gif_width,self.gif_height))))

                #print('[INFO]: UPDATING VEHICLE ENTRY ') #remove in stable
                #box_area=(xmax-xmin)*(ymax-ymin)
                #print('back box area:',box_area)
                #print('{} found'.format(text))
                #print('\n')
                #print('**********NUM AXLES FOUND:{}**********'.format(len(np.unique(np.array(self.axle_ids)))))                
                self.info['avcc_status']=True
                self.first_axle_time = str(datetime.now())
                self.info['time']=self.first_axle_time
                
                
                self.info['vehicle_back_image']=frame
                self.info['axle_count']=len(np.unique(np.array(self.axle_ids)))

                
              
                if 1<=(len(np.unique(np.array(self.axle_ids))))<=3:

                  print('\n')
                  print('**********NUM AXLES FOUND:{}**********'.format(self.info['axle_count']))
                  print('**********')
                  print('\n')
                  print('FPS:',self.calc_fps())
                  print('********')
                  print('\n')

                  ''' LIGHT VEHICLE MODE'''
                
                  #print('**********END OF VEHICLE FOUND MERGING IN LIGHT MODE *******')
                  self.info['merged_frame']=self.merged_frame_light
                  start_time=time()
                  mask=self.detect_and_segment.mask(self.merged_frame_light)
                  end_time=time()

                  if int(self.curr_type[-1]-1)==1:
                    self.info['vehicle_class']='car'
                  else:
                    self.info['vehicle_class']=self.detect_and_segment.vehicle_class
                    
                  name=self.info['vehicle_class']+'_num_axle_{}'.format(len(np.unique(np.array(self.axle_ids))))+'_'+self.first_axle_time+'.jpg'
                  self.info['video_path']=name[:-4]+'.gif'
                  print('**********VEHICLE NAME:{}**********'.format(name))
                  print('\n')
                  print('**********')
                  print('Time Taken for Light Mask Model:',end_time-start_time)
                  print('**********')
                  print('\n')
                  mask=cv2.resize(mask,(frame.shape[1],frame.shape[0]))
                  self.info['mask_bw']=mask.copy()
                  mask[mask>0]=1
                  mask=np.expand_dims(1-mask,axis=-1)
                  masked=self.info['merged_frame']*mask
                  self.info['mask_rgb']=masked.astype(np.uint8)
                  
              
                else:

                  ''' HEAVY VEHICLE MODE'''

                  print('\n')
                  print('[INFO]: Updating axle count for heavy vehicle mode')
                  print('\n')
                  self.info['axle_count']=len(self.merge_frames)

                  print('\n')
                  print('**********NUM AXLES FOUND:{}**********'.format(self.info['axle_count']))
                  print('**********')
                  print('\n')
                  print('FPS:',self.calc_fps())
                  print('********')
                  print('\n')

                  ######## checking axle count here also and switch to light or merge ##########

                  if self.info['axle_count']>3:

                     
                    self.info['merged_frame']=self.merge_and_clear_frames(self.merge_frames)
                     

                  else:

                    print('[INFO]: SWITCHING BACK TO LIGHT MODE')

                    self.info['merged_frame']=self.merged_frame_light

                  ######## checking axle count here also and switch to light or merge ##########

                  start_time=time()
                  mask=self.detect_and_segment.mask(self.info['merged_frame'])
                  end_time=time()

                  if int(self.curr_type[-1]-1)==1:
                    self.info['vehicle_class']='car'
                  else:
                    self.info['vehicle_class']=self.detect_and_segment.vehicle_class
                    
                  name=self.info['vehicle_class']+'_num_axle_{}'.format(self.info['axle_count'])+'_'+self.first_axle_time+'.jpg'
                  self.info['video_path']=name[:-4]+'.gif'
                  print('**********VEHICLE NAME:{}**********'.format(name))
                  print('\n')
                  print('**********')
                  if self.info['axle_count']>3:
                    print('Time Taken for Heavy Mask Model:',end_time-start_time)
                  else:
                    print('Time Taken for Light Mask Model:',end_time-start_time)
                  print('**********')
                  print('\n')
                  mask=cv2.resize(mask,(self.info['merged_frame'].shape[1],self.info['merged_frame'].shape[0]))
                  self.info['mask_bw']=mask.copy()
                  mask[mask>0]=1
                  mask=np.expand_dims(1-mask,axis=-1)
                  masked=self.info['merged_frame']*mask
                  self.info['mask_rgb']=masked.astype(np.uint8)
                  
                if self.write_gif:
                  
                  start_time=time()
                  
                  if len(self.gif_frames)>self.num_gif_frames_limit:
                    gif_interval=len(self.gif_frames)//self.num_gif_frames_limit
                  else:
                    gif_interval=1

                  optimized_gif_frames=[]

                  for i,gif in enumerate(self.gif_frames):
                    if i%gif_interval==0:
                      #print('gif id:',i)
                      optimized_gif_frames.append(gif)

                  print('********** SAVING GIF AFTER OPTIMIZING GIF FRAMES FROM {} to {} **********'.format(len(self.gif_frames),len(optimized_gif_frames)))
                  if len(optimized_gif_frames) >=2:
                    optimized_gif_frames[0].save(self.gif_write_path+'/{}.gif'.format(name[:-4]),save_all = True, append_images = optimized_gif_frames[1:], optimize = True, duration = 10,loop=0)
                    end_time=time()
                    print('\n')
                    print('Time Taken   for GIF creation :', end_time-start_time)
                    print('\n')

                print('[INFO]: MAX VEHICLE FRONTS FOUND: ',len(self.curr_type))
                print('[INFO]: RESET ATTRIBUTES ')
                '''
                RESET ATTRIBUTES
                '''
                self.curr_axle_id=-1
                self.write_light=False
                self.curr_type=[]
                self.axle_ids=[]
                self.written_ids=[]
                self.ocr=None
                self.number_plate_frame=None
              
                self.gif_frames=[]
                self.merge_frames={}

                self.info['ocr_frame_required']=False
                print('[INFO]: Disable OCR Required')

                #os.system('rm -r '+self.temp_file_path+'/*')
                #os.system('rm -r '+self.temp_gif_path+'/*')
        
            
          else:
            #if int((ymin+ymax)/2)>int(0*frame_width):
            axle_rects.append([xmin,ymin,xmax,ymax])

        
        
      start_time=time()
      objects,object_rects = self.axle_ct.update(axle_rects) ## update coords of detected axles to centroid ID assignment
      end_time=time()
      #print('Time Taken for Axle Counter update:',end_time-start_time)
      try:
      
        for (objectID, coords) in object_rects.items():
          centroid=[int((coords[0]+coords[2])/2),int((coords[1]+coords[3])/2)]
          width=coords[2]-coords[0]
          text = "axle:{}".format(objectID)
          self.curr_axle_id=objectID

          if self.write_gif:
                  self.gif_frames.append(Image.fromarray(cv2.resize(cv2.cvtColor(frame,cv2.COLOR_BGR2RGB),(self.gif_width,self.gif_height)))) 
                
          if self.curr_axle_id<=1:
              self.axle_ids.append(self.curr_axle_id)
              #print(self.write_video , self.is_video_writing , self.curr_type)
              
                

              if not self.write_light and centroid[0]<int(0.4*frame.shape[1]) and self.curr_type[-1]!=2:
                print(' ************ WRITING LIGHT FRAME FOR NON-CAR ***********')


                if self.write_gif:
                  self.gif_frames.append(Image.fromarray(cv2.resize(cv2.cvtColor(frame,cv2.COLOR_BGR2RGB),(self.gif_width,self.gif_height))))
                
                
                self.merged_frame_light=frame
                if self.curr_axle_id==1:
                  self.write_light=True

              if len(self.curr_type)>1:

                if not self.write_light and centroid[0]>=int(0.3*frame.shape[1]) and self.curr_type[-1]==2:
                  print(' ************ WRITING LIGHT FRAME FOR CAR AT {} AXLE {} ***********'.format(str(int(centroid[0])),str(self.curr_axle_id)))


                  if self.write_gif:
                    self.gif_frames.append(Image.fromarray(cv2.resize(cv2.cvtColor(frame,cv2.COLOR_BGR2RGB),(self.gif_width,self.gif_height))))
                
                
                  self.merged_frame_light=frame
                  if self.curr_axle_id==1:
                    print('[INFO]: ************ LIGHT FRAME FOR CAR FIXED AT {} AXLE {} ***********'.format(str(int(centroid[0])),str(self.curr_axle_id)))
                    self.write_light=True

          if int(0.5*frame.shape[1])<centroid[0]<int(0.65*frame.shape[1])and len(self.curr_type)>0:
              self.axle_ids.append(self.curr_axle_id)
          
              if self.curr_axle_id<1 :
                print("SAVE FRAME {}".format(str(self.curr_axle_id)))

                if self.write_gif:
                  self.gif_frames.append(Image.fromarray(cv2.resize(cv2.cvtColor(frame,cv2.COLOR_BGR2RGB),(self.gif_width,self.gif_height))))


                self.merge_frames['{}'.format(str(self.curr_axle_id))]=frame[:,int(0.5*frame.shape[1]):int(1.0*frame.shape[1])]
                #self.merge_frames.append(frame[:,int(0.5*frame.shape[1]):int(1.0*frame.shape[1])])
                #cv2.imwrite(self.temp_file_path+'/{}.jpg'.format(str(self.curr_axle_id)),frame[:,int(0.5*frame.shape[1]):int(1.0*frame.shape[1])])

              else:
            
                if self.curr_axle_id not in self.written_ids:
                  print("SAVE FRAME {}".format(str(self.curr_axle_id)))

                  if self.write_gif:
                    self.gif_frames.append(Image.fromarray(cv2.resize(cv2.cvtColor(frame,cv2.COLOR_BGR2RGB),(self.gif_width,self.gif_height))))
                  
                  self.merge_frames['{}'.format(str(self.curr_axle_id))]=frame[:,int(coords[0]):int(coords[2])]
                  #self.merge_frames.append(frame[:,int(coords[0]):int(coords[2])])
                  #cv2.imwrite(self.temp_file_path+'/{}.jpg'.format(str(self.curr_axle_id)),frame[:,int(coords[0]):int(coords[2])])
                  self.written_ids.append(self.curr_axle_id)


      except Exception as e:
          print(e , "in backend")
          traceback.print_exc()
    self.avcc_frame_counter+=1
    #process_end_time=time()
    #print('AVCC time:',process_end_time-process_start_time)
    return self.info
    '''
      AVCC Boolean : Response of AVCC if complete
      AVCC Response
        {
          num_axle:
          time:
          vehicle_front_image: np array
          vehicle_class: string
          merged_image: np array
          profile_bw  : np array
          profile_rgb : np array
          reference_video: string_path
        }
    '''
'''
#Usage:

if __name__=="__main__":
        
  videopath='/home/vignesh/vk/intozi/image_stitching/debug/videos/2021-08-22 00_46_57.236766camera1.mkv'
  #videopath=0
  cap=cv2.VideoCapture(videopath)
  avcc=AVCC(write_video=True)
  while cap.isOpened():

    ret,frame_=cap.read()
    
    if ret:
      frame_=cv2.resize(frame_,(int(384*1.5),int(384*1.5)))
      show_frame=frame_.copy()
      result=avcc.process(ret,frame_)
      print('status:',result['avcc_status'])
      if result['avcc_status']:
        print('vehicle_class:',result['vehicle_class'])
        print('axle_count:',result['axle_count'])
        print('OCR:',result['ocr'])
        
        cv2.imwrite('masked/{}_{}_{}.jpg'.format(result['vehicle_class'],result['axle_count'],result['time']),result['mask_rgb'])
        cv2.imwrite('mask/{}_{}_{}.jpg'.format(result['vehicle_class'],result['axle_count'],result['time']),result['mask_bw'])
        cv2.imwrite('merged/{}_{}_{}.jpg'.format(result['vehicle_class'],result['axle_count'],result['time']),result['merged_frame'])
        cv2.imwrite('vehicle_front_image/{}_{}_{}.jpg'.format(result['vehicle_class'],result['axle_count'],result['time']),result['vehicle_front_image'])
        cv2.imwrite('vehicle_back_image/{}_{}_{}.jpg'.format(result['vehicle_class'],result['axle_count'],result['time']),result['vehicle_back_image'])
        if hasattr(result['number_plate_frame'],'shape'):
          
          cv2.imwrite('number_plate/{}_{}_{}_{}.jpg'.format(result['vehicle_class'],result['axle_count'],result['ocr'],result['time']),result['number_plate_frame'])
      cv2.line(show_frame,(int(0.5*show_frame.shape[1]),0),(int(0.5*show_frame.shape[1]),show_frame.shape[0]),(255,0,0),1)
      cv2.line(show_frame,(int(0.65*show_frame.shape[1]),0),(int(0.65*show_frame.shape[1]),show_frame.shape[0]),(255,0,0),1)
    else:
      print('End of File')
      break
    #cv2.imshow('out',show_frame)
    #if cv2.waitKey(1) & 0xFF == ord('q'):
    #  break

  cap.release()
'''
