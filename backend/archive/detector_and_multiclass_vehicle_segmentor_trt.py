'''
Copyright 2021 Vignesh Kotteeswaran <iamvk888@gmail.com>
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''

'''
Load the keras SSD detection model and give out the filtered
bounding box predictions
'''

import cv2
import tensorflow as tf
import numpy as np
from tensorflow.python.compiler.tensorrt import trt_convert as trt
from tensorflow.keras.models import load_model
from assets.ssd_output_decoder import decode_detections
from assets.utils import box_filter


class Detector_and_MultiClass_VehicleSegmentor():
    def __init__(self,modelpath,maskmodelpath,img_width,img_height):
        self.img_width=img_width
        self.img_height=img_height
        
        self.blob=cv2.cvtColor(cv2.imread('calibrate.jpg'),cv2.COLOR_BGR2RGB)

        self.load_detector(modelpath)
        self.load_mask_model(maskmodelpath)
               
        self.classes=['background','axle','bus','car','lcv','mav','truck']
        self.vehicle_class=None
        
        self.normalize_coords=True

        
        
        
    
    def load_detector(self,modelpath):
        self.root=tf.saved_model.load(modelpath)
        self.infer=self.root.signatures['serving_default']
        self.infer(input_1=np.expand_dims(cv2.resize(self.blob,(self.img_width,self.img_height)),axis=0).astype(np.float32))
        
        print('\n')
        print('***** DETECTOR MODEL LOADING COMPLETED WITH WARMUP *****')
        print('\n')


    def load_mask_model(self,maskmodelpath):
        self.mask_root=tf.saved_model.load(maskmodelpath)
        self.mask_infer=self.mask_root.signatures['serving_default']
        self.mask_infer(input_1=np.expand_dims(cv2.resize(self.blob,(256,256)),axis=0).astype(np.float32))

        print('\n')
        print('***** SEGMENTATION MODEL LOADING COMPLETED WITH WARMUP *****')
        print('\n')



    def detect(self,img,filter_thresh,confidence_thresh,iou_threshold,filter_=True):
        img=np.expand_dims(cv2.resize(img,(self.img_width,self.img_height)),axis=0).astype(np.float32)
        pred=self.infer(input_1=img)['predictions'].numpy()
        pred=np.expand_dims(pred[pred[:,:,0]<1.0-confidence_thresh],axis=0)
        
        self.dpred=decode_detections(pred,confidence_thresh=confidence_thresh,iou_threshold=iou_threshold,top_k=200,input_coords='centroids',normalize_coords=self.normalize_coords,img_height=self.img_height,img_width=self.img_width)[0]
        
        if filter_:
            self.dpred=box_filter(self.dpred,filter_thresh)
        
        if len(self.dpred.shape)>1:
            return self.dpred
                
        else:
            return 'No vehicle found'

    def mask(self,img):
        img=np.expand_dims(cv2.resize(img,(256,256)),axis=0).astype(np.float32)
        out=self.mask_infer(input_1=img)['activation_51'].numpy()
        
        max_out=np.argmax(out,axis=-1)
        self.vehicle_class=self.classes[np.unique(max_out,return_counts=True)[0].max()]
        if self.vehicle_class=='Background'or self.vehicle_class=='Axle':
            self.vehicle_class=='NA'
        mask=max_out.squeeze()
        mask[mask>0]=255
        inv_mask=255-mask
        return np.uint8(inv_mask)
    
