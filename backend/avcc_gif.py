'''
Copyright 2021 Vignesh Kotteeswaran <Intozi Tech Pself Ltd,vignesh@intozi.io>
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''

import cv2
import traceback
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf

gpus = tf.config.list_physical_devices('GPU')
if gpus:
  try:
    # Currently, memory growth needs to be the same across GPUs
    for gpu in gpus:
      #tf.config.experimental.set_memory_growth(gpu, True)
      tf.config.set_logical_device_configuration(
      gpu,
      [tf.config.LogicalDeviceConfiguration(memory_limit=128)])
      
    logical_gpus = tf.config.list_logical_devices('GPU')
    print('\n')
    print('***** Limiting GPU Growth *****')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    print('\n')
  except RuntimeError as e:
    # Memory growth must be set before GPUs have been initialized
    print(e)

from .centroidtracker import CentroidTracker
from .detector_and_multiclass_vehicle_detector_trt import Detector_and_MultiClass_VehicleDetector
from tensorflow.keras import backend as K
import numpy as np
from tensorflow.keras.preprocessing.image import img_to_array,load_img
from matplotlib import pyplot as plt
from time import time
import pandas as pd
import os
from glob import glob
from datetime import datetime
from shutil import copy
from .avcc_profile import AVCC_PROFILE
from .ocr_trt import OCR
from .number_plate_trt import Number_Plate
import shutil
from PIL import Image

### remove this in production ###
from random import choice

### remove this in production ###

#managed by Backend
#if not(os.path.exists('videos')):
#  os.mkdir('videos')

#managed by Backend
#if not(os.path.exists('mask')):
#  os.mkdir('mask')


#managed by Backend
#if not(os.path.exists('merged')):
#  os.mkdir('merged')


class AVCC():

  def __init__(self,anpr_frame_queue=None , anpr_response_queue = 'None',write_gif=True , gif_write_path = '/var/www/html/videos/' ):

    '''
    Please check for folder 'gifs' , 'gif_frames'
    '''
    '''
    self.temp_file_path = os.getcwd()+'/backend/frames'
    
    if not(os.path.exists(self.temp_file_path)):
      os.mkdir(self.temp_file_path)

    else:
      os.system('rm -r '+self.temp_file_path+'/*')
    os.system("sudo chmod 777 "+ self.temp_file_path)
      #shutil.rmtree(self.temp_file_path+'/')
    
    
    self.temp_gif_path = os.getcwd()+'/backend/gif_frames'
    
    if not(os.path.exists(self.temp_gif_path)):
      os.mkdir(self.temp_gif_path)

    else:
      os.system('rm -r '+self.temp_gif_path+'/*')
      #shutil.rmtree(self.temp_file_path+'/')
      
    os.system("sudo chmod 777 "+ self.temp_gif_path)
    '''

    start=time()
    
    self.img_width=384
    self.img_height=256
    self.gif_width=192
    self.gif_height=96
    self.num_gif_frames_limit=30
    self.curr_axle_id=-1
    self.curr_type=[]
    self.axle_ct = CentroidTracker(0)
    self.write_light=False
    self.axle_ids=[]
    self.written_ids=[]
    self.write_gif=write_gif
    self.anpr_frame_queue = anpr_frame_queue
    self.ocr=None
    self.number_plate_frame=None
    
    self.info={}
    self.gif_write_path = gif_write_path
    self.anpr_response_queue = anpr_response_queue
    self.info['ocr_frame_required']=False
    self.info['avcc_status']=False
    
    self.ocr_frame=None
    self.gif_frames=[]
    self.merge_frames={}
    K.clear_session()
    #self.detect_and_segment=Detector_and_Segmentor(modelpath='axle_front_back20.h5',maskmodelpath='vehicle_axle_segment6.h5',size=(self.img_width,self.img_height))
    self.detect_and_classify=Detector_and_MultiClass_VehicleDetector(stage1path=os.getcwd()+'/files/'+'axle_front_back_others_32_mAP90.0_axleAP90.26_backAP88.17_nano2gb',stage2path=os.getcwd()+'/files/'+'stage2_vehdetect_v1_313233_mAP70_nano2gb',img_width=self.img_width,img_height=self.img_height)
    
    self.number_plate_model=Number_Plate()
    self.avcc_profile_model=AVCC_PROFILE()

    print('Number Plate model loaded')
    end=time()
    print('Time Elapsed for Init & Warmup:',end-start)
   
    self.avcc_frame_counter=0
    self.avcc_start_time=time()
    '''
    Line values
    cv2.line(show_frame,(int(0.5*frame.shape[1]),0),(int(0.5*frame.shape[1]),frame.shape[0]),(255,0,0),1)
    cv2.line(show_frame,(int(0.65*frame.shape[1]),0),(int(0.65*frame.shape[1]),frame.shape[0]),(255,0,0),1)'''
    

  def merge_and_clear_frames(self,merge_frames):
    
    if len(merge_frames)<1:
        return

    frames=[]
    frame_widths=[]
    for key_ in sorted(merge_frames.keys(),reverse=True):
      #print('merge key:',key_)
      img=merge_frames[key_]
      frame_height=img.shape[0]
      frame_width=img.shape[1]
      frames.append(img)
      frame_widths.append(frame_width)

    merged_frame=np.zeros((frame_height,sum(frame_widths),3))
    start_width=0
    for img,width in zip(frames,frame_widths):
      end_width=start_width+width
      merged_frame[:,start_width:end_width]=img
      start_width=end_width
      
    return merged_frame

  def classify_vehicle_fromstage2result(self,dpred,frame_width,frame_height):

    print('stage 2 dpred:',dpred)

    box_confidence=[]
    box_classes=[]

    if hasattr(dpred,'shape'): ## check if detections are not empty
        for class_,score,coords in zip(dpred[:,0],dpred[:,1],dpred[:,-4:]):

          coords=[int((coords[0]/self.img_width)*frame_width),int((coords[1]/self.img_height)*frame_height),int((coords[2]/self.img_width)*frame_width),int((coords[3]/self.img_height)*frame_height)]
          xmin,ymin,xmax,ymax=[i for i in coords]
          #box_areas.append((ymax-ymin)*(xmax-xmin))
          box_confidence.append(score)
          box_classes.append(int(class_))

    else:
      return 'No_Class'

    max_conf_index=int(np.argmax(box_confidence))
    return self.detect_and_classify.classes[box_classes[max_conf_index]]

  def do_ocr(self,ocr_frame):
    #pass
    
    number_plate_infos=self.number_plate_model.detection(ocr_frame)
    #print('number_plate_info:',number_plate_infos)
    for info in number_plate_infos:
      x,y,w,h,text,conf=info
      self.number_plate_frame=ocr_frame[y:y+h,x:x+w]
      self.ocr=text#self.ocr_model.detection(self.number_plate_frame)

    self.info['ocr_frame_required']=False

    print('[INFO]: OCR DONE & Disableds OCR Required')
    
  '''
  entering the brain
  '''
  def calc_fps(self):
    return self.avcc_frame_counter//(time()-self.avcc_start_time)

  def process(self,ret,  line_param1 , line_param2, anpr_calibration, frame=None):
  
    '''
    Args:
    ret: Boolean returned by OpenCV video reader
    frame: numpy array of image
    ''' 
    #print(frame.shape)
    self.process_start_time=time()
    try:
        self.ocr_frame = self.anpr_frame_queue.get_nowait()
    except:
        pass
    if self.info['ocr_frame_required']:
      try:
        #temp_frame=self.anpr_frame_queue.get_nowait()
        
        start_time=time()
        self.do_ocr(self.ocr_frame)
        end_time=time()
        #print('\n')
        print('Time Taken for OCR:',end_time-start_time)
        info = {}
        if self.ocr == None:
          self.ocr="None"
        #info['ocr_frame_required']=False
        info['status'] = True
        info['vehicle_front_image']=self.ocr_frame
        info['ocr']=self.ocr
        info['number_plate_frame']=self.number_plate_frame
        info['created_datetime'] = datetime.now()
        #cv2.imwrite(str(datetime.now())+"_"+self.ocr+".jpg" , self.ocr_frame)
        try:
          self.anpr_response_queue.put_nowait(info)
        except Exception as anpr_nowait_failed1:
          print('[ERROR]:anpr_nowait_failed1 ',anpr_nowait_failed1)
          pass

        #print('********** OCR Done *********')
        #print('\n')
      except Exception as failed:
        print(failed)
        #cv2.imwrite(str(datetime.now())+"_failed_"+self.ocr+".jpg" , self.ocr_frame)
        self.ocr_frame = frame
        info = {}
        info['status'] = True
        info['vehicle_front_image']=None#self.ocr_frame
        info['ocr']='None'
        info['number_plate_frame']=None#self.number_plate_frame
        info['created_datetime'] = datetime.now()
        try:
          self.anpr_response_queue.put_nowait(info)
        except Exception as anpr_nowait_failed2:
          print('[ERROR]:anpr_nowait_failed2 ',anpr_nowait_failed2)
          pass

        
      
      
    self.info={}
    self.info['ocr_frame_required']=False
    self.info['avcc_status']=False

    if ret == True:
      #process_start_time=time()
      #frame=cv2.resize(frame,(int(384*1.5),int(384*1.5)))
      frame_height,frame_width=frame.shape[0],frame.shape[1]
    
      blob=cv2.cvtColor(frame,cv2.COLOR_BGR2RGB).astype(np.float32)
      stage1_start_time=time()
      dpred=self.detect_and_classify.detect(self.detect_and_classify.stage1model,blob,filter_=False,filter_thresh=0.1,iou_threshold=0.1,confidence_thresh=0.9)
      stage1_end_time=time()
      #print('stage1 inference time:',end_time-start_time)
      axle_rects=[]     # collects list of axle rectangle bounding box in the image
      if hasattr(dpred,'shape'): ## check if detections are not empty
        for class_,score,coords in zip(dpred[:,0],dpred[:,1],dpred[:,-4:]):
          coords=[int((coords[0]/self.img_width)*frame_width),int((coords[1]/self.img_height)*frame_height),int((coords[2]/self.img_width)*frame_width),int((coords[3]/self.img_height)*frame_height)]
          xmin,ymin,xmax,ymax=[i for i in coords]
          centroid=[int((xmin+xmax)/2),int((ymin+ymax)/2)]
        
          if class_!=6:
            #text=self.classes[int(class_)-1]
            if class_<5 and centroid[0]<=int(0.65*frame.shape[1]):

              if self.write_gif:
                  self.gif_frames.append(Image.fromarray(cv2.resize(cv2.cvtColor(frame,cv2.COLOR_BGR2RGB),(self.gif_width,self.gif_height))))

              self.curr_type.append(class_)
              self.axle_ids=[]
              self.vehicle_front_image=frame
              if len(self.curr_type)>3:
                #print("Going in for anpr ")
                if len(self.curr_type)==anpr_calibration:# earlier detection for ANPR default is 2
                  print("INFO :: OCR Required " , anpr_calibration) # erase                   
                  self.info['ocr_frame_required']=True

              self.axle_ct = CentroidTracker(0)
                
          
            if class_==5 and centroid[0]>=int(0.65*frame.shape[1]):

              if len(self.curr_type)>0 and len(self.axle_ids)>0:
                if self.write_gif:
                  self.gif_frames.append(Image.fromarray(cv2.resize(cv2.cvtColor(frame,cv2.COLOR_BGR2RGB),(self.gif_width,self.gif_height))))
                #box_area=(xmax-xmin)*(ymax-ymin)
                #print('back box area:',box_area)
                #print('{} found'.format(text))

                self.info['avcc_status']=True
                self.first_axle_time = datetime.now()
                self.info['time']=self.first_axle_time
                
                
                self.info['vehicle_back_image']=frame
                self.info['axle_count']=len(np.unique(np.array(self.axle_ids)))

                
              
                if 1<=(len(np.unique(np.array(self.axle_ids))))<=3:

                  print('\n')
                  print('**********NUM AXLES FOUND:{}**********'.format(self.info['axle_count']))
                  print('**********')
                  print('\n')
                  print('FPS:',self.calc_fps(),'_','Frame shape:',frame.shape)
                  print('********')
                  print('\n')

                  ''' LIGHT VEHICLE MODE'''
                
                  #print('**********END OF VEHICLE FOUND MERGING IN LIGHT MODE *******')
                  self.info['merged_frame']=self.merged_frame_light
                  start_time=time()
                  #stage 2 prediction here
                  print(self.info['merged_frame'].shape)
                  #stage2dpred=self.detect_and_classify.detect(self.detect_and_classify.stage2model,cv2.cvtColor(self.info['merged_frame'],cv2.COLOR_BGR2RGB),filter_=False,filter_thresh=0.1,iou_threshold=0.1,confidence_thresh=0.65)
                  #self.info['vehicle_class']=self.classify_vehicle_fromstage2result(stage2dpred,frame_width,frame_height)
                  #print("INFO Testing Merge Frame Before :: ------------------" , self.info['merged_frame'].dtype)
                  #print(type(self.info['merged_frame']))
                  if self.info['merged_frame'].dtype != np.uint8:
                    self.info['merged_frame'] = self.info['merged_frame'].astype(np.uint8)
                  #print("INFO Testing Merge Frame After :: ------------------" , self.info['merged_frame'].dtype)
                  #print(type(self.info['merged_frame']))
                  self.info['vehicle_class'] = self.avcc_profile_model.process(self.info['merged_frame'])
                  end_time=time()
                    
                  name=self.info['vehicle_class']+'_num_axle_{}'.format(len(np.unique(np.array(self.axle_ids))))+'_'+str(self.first_axle_time)+'.jpg'
                  self.info['video_path']=name[:-4]+'.gif'
                  print('**********VEHICLE NAME:{}**********'.format(name))
                  print('\n')
                  print('**********')
                  print('Time Taken for Stage1 Model:',stage1_end_time-stage1_start_time)
                  print('Time Taken for Stage2 Model:',end_time-start_time)
                  print('**********')
                  print('\n')
                  
                  
              
                else:

                  ''' HEAVY VEHICLE MODE'''

                  print('\n')
                  print('[INFO]: Updating axle count for heavy vehicle mode')
                  print('\n')
                  self.info['axle_count']=len(self.merge_frames)

                  print('\n')
                  print('**********NUM AXLES FOUND:{}**********'.format(self.info['axle_count']))
                  print('**********')
                  print('\n')
                  print('FPS:',self.calc_fps())
                  print('********')
                  print('\n')

                  ######## checking axle count here also and switch to light or merge ##########

                  if self.info['axle_count']>3:
                    print('[INFO]: CONTINUING IN HEAVY MODE') 
                    self.info['merged_frame']=self.merge_and_clear_frames(self.merge_frames)
                     
                  else:
                    print('[INFO]: SWITCHING BACK TO LIGHT MODE')
                    self.info['merged_frame']=self.merged_frame_light

                  ######## checking axle count here also and switch to light or merge ##########

                  start_time=time()
                  #stage 2 prediction here
                  print(self.info['merged_frame'].shape)
                  #stage2dpred=self.detect_and_classify.detect(self.detect_and_classify.stage2model,self.info['merged_frame'],filter_=False,filter_thresh=0.1,iou_threshold=0.1,confidence_thresh=0.35)
                  #self.info['vehicle_class']=self.classify_vehicle_fromstage2result(stage2dpred,frame_width,frame_height)
                  print("INFO Testing Merge Frame Before :: ------------------" , self.info['merged_frame'].dtype)
                  print(type(self.info['merged_frame']))
                  if self.info['merged_frame'].dtype != np.uint8:
                    self.info['merged_frame'] = self.info['merged_frame'].astype(np.uint8)
                  print("INFO Testing Merge Frame After :: ------------------" , self.info['merged_frame'].dtype)
                  print(type(self.info['merged_frame']))

                  self.info['vehicle_class'] = self.avcc_profile_model.process(self.info['merged_frame'])

                  end_time=time()
                    
                  name=self.info['vehicle_class']+'_num_axle_{}'.format(self.info['axle_count'])+'_'+str(self.first_axle_time)+'.jpg'
                  self.info['video_path']=name[:-4]+'.gif'
                  print('**********VEHICLE NAME:{}**********'.format(name))
                  print('\n')
                  print('**********')                  
                  print('Time Taken for Stage1 Model:',stage1_end_time-stage1_start_time)
                  print('Time Taken for Stage2 Model:',end_time-start_time)
                  print('**********')
                  print('\n')
                  
                  
                if self.write_gif:
                  
                  start_time=time()
                  
                  if len(self.gif_frames)>self.num_gif_frames_limit:
                    gif_interval=len(self.gif_frames)//self.num_gif_frames_limit
                  else:
                    gif_interval=1

                  optimized_gif_frames=[]

                  for i,gif in enumerate(self.gif_frames):
                    if i%gif_interval==0:
                      #print('gif id:',i)
                      optimized_gif_frames.append(gif)

                  print('********** SAVING GIF AFTER OPTIMIZING GIF FRAMES FROM {} to {} **********'.format(len(self.gif_frames),len(optimized_gif_frames)))
                  if len(optimized_gif_frames) >=2:
                    optimized_gif_frames[0].save(self.gif_write_path+'/{}.gif'.format(name[:-4]),save_all = True, append_images = optimized_gif_frames[1:], optimize = True, duration = 10,loop=0)
                    end_time=time()
                    print('\n')
                    print('Time Taken   for GIF creation :', end_time-start_time)
                    print('\n')

                print('[INFO]: MAX VEHICLE FRONTS FOUND: ',len(self.curr_type))
                print('[INFO]: RESET ATTRIBUTES ')
                '''
                RESET ATTRIBUTES
                '''
                self.curr_axle_id=-1
                self.write_light=False
                self.curr_type=[]
                self.axle_ids=[]
                self.written_ids=[]
                self.ocr=None
                self.number_plate_frame=None
              
                self.gif_frames=[]
                self.merge_frames={}
                #self.info['merged_frame']=None

                self.info['ocr_frame_required']=False
                print('[INFO]: Disable OCR Required')

                #os.system('rm -r '+self.temp_file_path+'/*')
                #os.system('rm -r '+self.temp_gif_path+'/*')
        
            
          else:
            #if int((ymin+ymax)/2)>int(0*frame_width):
            axle_rects.append([xmin,ymin,xmax,ymax])

        
        
      start_time=time()
      objects,object_rects = self.axle_ct.update(axle_rects) ## update coords of detected axles to centroid ID assignment
      end_time=time()
      #print('Time Taken for Axle Counter update:',end_time-start_time)
      try:
      
        for (objectID, coords) in object_rects.items():
          centroid=[int((coords[0]+coords[2])/2),int((coords[1]+coords[3])/2)]
          width=coords[2]-coords[0]
          text = "axle:{}".format(objectID)
          self.curr_axle_id=objectID

          
                
          if self.curr_axle_id<=1:
              self.axle_ids.append(self.curr_axle_id)
              #print(self.write_video , self.is_video_writing , self.curr_type)
              
              if not self.write_light and centroid[0]<int(0.5*frame.shape[1]):
                print(' ************ WRITING LIGHT FRAME ***********')

                if self.write_gif:
                  self.gif_frames.append(Image.fromarray(cv2.resize(cv2.cvtColor(frame,cv2.COLOR_BGR2RGB),(self.gif_width,self.gif_height))))               
                
                self.merged_frame_light=frame
                if self.curr_axle_id==1 and centroid[0]>int(0.3*frame.shape[1]):
                    self.write_light=True

          if int(0.5*frame.shape[1])<centroid[0]<int(0.65*frame.shape[1])and len(self.curr_type)>0:
              self.axle_ids.append(self.curr_axle_id)
          
              if self.curr_axle_id<1 :
                print("SAVE FRAME {}".format(str(self.curr_axle_id)))
                if self.write_gif:
                  self.gif_frames.append(Image.fromarray(cv2.resize(cv2.cvtColor(frame,cv2.COLOR_BGR2RGB),(self.gif_width,self.gif_height))))
                self.merge_frames['{}'.format(str(self.curr_axle_id))]=frame[:,int(0.5*frame.shape[1]):int(1.0*frame.shape[1])]
                #self.merge_frames.append(frame[:,int(0.5*frame.shape[1]):int(1.0*frame.shape[1])])
                #cv2.imwrite(self.temp_file_path+'/{}.jpg'.format(str(self.curr_axle_id)),frame[:,int(0.5*frame.shape[1]):int(1.0*frame.shape[1])])

              else:
            
                if self.curr_axle_id not in self.written_ids:
                  print("SAVE FRAME {}".format(str(self.curr_axle_id)))
                  if self.write_gif:
                    self.gif_frames.append(Image.fromarray(cv2.resize(cv2.cvtColor(frame,cv2.COLOR_BGR2RGB),(self.gif_width,self.gif_height))))             
                  self.merge_frames['{}'.format(str(self.curr_axle_id))]=frame[:,int(coords[0]):int(coords[2])]
                  #self.merge_frames.append(frame[:,int(coords[0]):int(coords[2])])
                  #cv2.imwrite(self.temp_file_path+'/{}.jpg'.format(str(self.curr_axle_id)),frame[:,int(coords[0]):int(coords[2])])
                  self.written_ids.append(self.curr_axle_id)


      except Exception as e:
          print(e , "in backend")
          traceback.print_exc()
    self.avcc_frame_counter+=1
    self.process_end_time=time()
    #if self.info['avcc_status']:
    '''
    print('\n')
    print('**********')
    print('AVCC time:',self.process_end_time-self.process_start_time,'_',stage1_end_time-stage1_start_time,'_',frame.shape)
    print('**********')
    print('\n')'''
    return self.info
    '''
      AVCC Boolean : Response of AVCC if complete
      AVCC Response
        {
          num_axle:
          time:
          vehicle_front_image: np array
          vehicle_class: string
          merged_image: np array
          profile_bw  : np array
          profile_rgb : np array
          reference_video: string_path
        }
    '''
'''
#Usage:

if __name__=="__main__":
        
  videopath='/home/vignesh/vk/intozi/image_stitching/debug/videos/2021-08-22 00_46_57.236766camera1.mkv'
  #videopath=0
  cap=cv2.VideoCapture(videopath)
  avcc=AVCC(write_video=True)
  while cap.isOpened():

    ret,frame_=cap.read()
    
    if ret:
      frame_=cv2.resize(frame_,(int(384*1.5),int(384*1.5)))
      show_frame=frame_.copy()
      result=avcc.process(ret,frame_)
      print('status:',result['avcc_status'])
      if result['avcc_status']:
        print('vehicle_class:',result['vehicle_class'])
        print('axle_count:',result['axle_count'])
        print('OCR:',result['ocr'])
        
        cv2.imwrite('masked/{}_{}_{}.jpg'.format(result['vehicle_class'],result['axle_count'],result['time']),result['mask_rgb'])
        cv2.imwrite('mask/{}_{}_{}.jpg'.format(result['vehicle_class'],result['axle_count'],result['time']),result['mask_bw'])
        cv2.imwrite('merged/{}_{}_{}.jpg'.format(result['vehicle_class'],result['axle_count'],result['time']),result['merged_frame'])
        cv2.imwrite('vehicle_front_image/{}_{}_{}.jpg'.format(result['vehicle_class'],result['axle_count'],result['time']),result['vehicle_front_image'])
        cv2.imwrite('vehicle_back_image/{}_{}_{}.jpg'.format(result['vehicle_class'],result['axle_count'],result['time']),result['vehicle_back_image'])
        if hasattr(result['number_plate_frame'],'shape'):
          
          cv2.imwrite('number_plate/{}_{}_{}_{}.jpg'.format(result['vehicle_class'],result['axle_count'],result['ocr'],result['time']),result['number_plate_frame'])
      cv2.line(show_frame,(int(0.5*show_frame.shape[1]),0),(int(0.5*show_frame.shape[1]),show_frame.shape[0]),(255,0,0),1)
      cv2.line(show_frame,(int(0.65*show_frame.shape[1]),0),(int(0.65*show_frame.shape[1]),show_frame.shape[0]),(255,0,0),1)
    else:
      print('End of File')
      break
    #cv2.imshow('out',show_frame)
    #if cv2.waitKey(1) & 0xFF == ord('q'):
    #  break

  cap.release()
'''
