import pycuda.autoinit
from .yolo_with_plugins_avcc_stage1 import TrtYOLO
import cv2
import glob
import os
class AVCC_STAGE1 :
    def __init__(self) :
        #self.v_model = "avcc_stage_one-yolov4-tiny.trt"
        self.v_model =os.path.join(os.getcwd(),"files", "avcc_stage_one-yolov4-tiny.trt")

        self.v_trt_yolo = TrtYOLO(self.v_model, (384,384), 1)
        self.confidence_threshold = 0.25
        self.labels=['axle','front','back']
    
    def process(self,img) :
        boxes, confs, clss = self.v_trt_yolo.detect(img, self.confidence_threshold)

        #image  = img.copy()
        info =[]

        for bb, cf, cl in zip(boxes, confs, clss):
            cl = int(cl)
            x_min, y_min, x_max, y_max = bb[0], bb[1], bb[2], bb[3]
            #cv2.rectangle(img, (x_min, y_min), (x_max, y_max), (0,0,255), 2)
            text = self.labels[cl] 
            #print(text)
            info.append([x_min, y_min, x_max-x_min, y_max-y_min, text, cf])
        
        #cv2.imshow("", img)
        #cv2.waitKey(1)
        
        return info


if __name__ == "__main__" :
    import time

    v_cls = AVCC_STAGE1()
    cap = cv2.VideoCapture("/home/intozi/avcc_stage1/2021-10-14 22_45_14.291468.mp4")
    while cap.isOpened() :
        r,f = cap.read()
        s=time.time()
        v_cls.process(f)
        print("TIME : ", time.time()-s)
    '''
    #img = cv2.imread("/home/intozi/Downloads/NC/691be401-2d61-4944-ab95-7f522e43bd46.jpg")
    #for i in range(1) :
    for i in glob.glob("/home/intozi/Downloads/NC/*jpg") :
        img = cv2.imread(i)
        s=time.time()
        info = v_cls.process(img)
        e=time.time()
        print("TIME TAKEN :", e-s)
    print(info)
    '''

