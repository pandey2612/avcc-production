from . import yolo_with_plugins_ocr_rgb 
import cv2
import os
import pycuda.autoinit
import glob 
import time
from .ocr_deskew import deskew_plate
import numpy as np

class OCR :
    def __init__(self) :
        self.labels = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
        self.ocr_model = os.path.join(os.getcwd(),"files","ocr-yolov4-tiny-512x288.trt")
        self.ocr_trt_yolo = yolo_with_plugins_ocr_rgb.TrtYOLO(self.ocr_model, (288,512), 1)
        self.confidence_threshold = 0.3
        self.mapping = {}
        for i in range(10) :
            self.mapping[i]=i
        j=10
        for i in range(65,91) :
            self.mapping[j]=chr(i)
            j+=1
        #print(self.mapping)

    def xmin_sorter(self,dpred,return_arg=False):
        s=[i for i in np.argsort(dpred[:,-4])]
        ordered= np.array([dpred[i,:] for i in s])
        if return_arg:
            return ordered
        else:
            #print(ordered)
            return [self.labels[int(i)] for i in ordered[:,0]]

    def ymin_sorter(self,dpred,return_arg=False):
        s=[i for i in np.argsort(dpred[:,-3])]
        #print(s)
        ordered= np.array([dpred[i,:] for i in s])
        if return_arg:
            return ordered
        else:
            #print(ordered)
            return [self.labels[int(i)] for i in ordered[:,0]]

    def bb_sorter(self,dpred):
        if len(dpred)==0:
            return ''
        if len(dpred.shape)<2:
            dpred=np.expand_dims(dpred,axis=0)
        ysort=self.ymin_sorter(dpred,return_arg=True)
        x_sort=self.xmin_sorter(dpred,return_arg=True)

        if not((ysort[:,-1]>ysort[-1,-3]).all()):
            #print('double line')
            return self.xmin_sorter(ysort[ysort[:,-1]<ysort[-1,-3]])+self.xmin_sorter(ysort[ysort[:,-1]>ysort[-1,-3]])
        
        elif x_sort[0,-3]>x_sort[-1,-3]:
            return self.xmin_sorter(dpred)
    
        
        else:
            #print('single line')
            return self.xmin_sorter(dpred)
        

    def detection(self,img) :
        info = []
        text = None
        #img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        
        filtered_yolobox= self.ocr_trt_yolo.detect(img, self.confidence_threshold)
        #self.ocr_trt_yolo.detect(img, self.confidence_threshold)
        
        #print(filtered_yolobox)
        if len(filtered_yolobox) >0:
            filtered_yolobox[:,-4:]=deskew_plate(img,filtered_yolobox)
            outputs=self.bb_sorter(np.array(filtered_yolobox))
            #print(outputs)
            return ''.join(str(i) for i in outputs)

        else:
            return None
            
'''        
if __name__ ==  "__main__" :
    o = OCR()

    #p = "/home/intozi/Downloads/h2"
    for i in sorted(glob.glob("/home/intozi/intozi/Vignesh/ocr_trt_conversion/img/*.jpg")):
        print('file:',i)
        #i='img/7ff79c03-c399-4cc5-887d-a4c80512f7c6.jpg'
        img = cv2.imread(i)
        start=time.time()
        t = o.detection(img)
        end=time.time()
        print('out:',t)
        print('time consumed:',end-start)
        cv2.imshow("", img)
        cv2.waitKey()
'''        
