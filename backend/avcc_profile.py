import pycuda.autoinit
from .yolo_with_plugins_vehicle import TrtYOLO
import cv2
import glob
import os
class AVCC_PROFILE :
    def __init__(self) :
        self.v_model = os.path.join(os.getcwd(),"files", "avcc-profile-yolov4-tiny-custom.trt")

        self.v_trt_yolo = TrtYOLO(self.v_model, (416,416), 1)
        self.confidence_threshold = 0.25
        #self.classes=['Background','Hatchback','Jeep','LCV','MAV','Mini Bus','Mini Truck','Pickup Truck','SUV','Sedan','Three Wheelers','Tractor','Tractor-Trailer','Truck','Two wheelers','Van','Bus']
        
        self.labels=['Bus','Hatchback','Jeep','LCV','MAV','Mini Bus','Mini Truck','Pickup Truck','SUV','Sedan','Three Wheelers','Tractor',
                     'Tractor-Trailer','Truck','Two wheelers','Van']
    
    def process(self,img) :
        boxes, confs, clss = self.v_trt_yolo.detect(img, self.confidence_threshold)

        #image  = img.copy()
        info =[]
        text = 'None'
        for bb, cf, cl in zip(boxes, confs, clss):
            cl = int(cl)
            x_min, y_min, x_max, y_max = bb[0], bb[1], bb[2], bb[3]
            #cv2.rectangle(img, (x_min, y_min), (x_max, y_max), (0,0,255), 2)
            text = self.labels[cl] 
            #print(text)
            #info.append([x_min, y_min, x_max-x_min, y_max-y_min, text, cf])
            info.append(text)

        #cv2.imshow("", img)
        #cv2.waitKey()
        
        return text


if __name__ == "__main__" :
    import time

    v_cls = AVCC()
    img = cv2.imread("37_bus_2_2021-12-1916_55_48.136282.jpg")
    for i in range(15) :
        s=time.time()
        info = v_cls.process(img)
        e=time.time()
        print("TIME TAKEN :", e-s)
    print(info)

